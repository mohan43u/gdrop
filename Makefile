#Makefile for tweet

CC := gcc
GIRSCANNER := g-ir-scanner
GIRCOMPILER := g-ir-compiler

MODULES := gio-unix-2.0 oauth libsoup-2.4 json-glib-1.0
SOURCES := drop-oauth.c drop-soup.c drop-dropbox.c drop-object.c drop.c
HEADERS := $(SOURCES:%.c=%.h)
CFLAGS += $(shell pkg-config --cflags $(MODULES)) -I. -O0 -ggdb -fPIC
LIBS += $(shell pkg-config --libs $(MODULES)) -lreadline

BINARYOBJECTS := $(SOURCES:%.c=%.o)
BINARY := drop
LIBRARYOBJECTS := $(filter-out drop.o, $(BINARYOBJECTS))
LIBRARY := libdrop.so

GIRPKGS := $(MODULES:%=--pkg=%)
GIRNAMESPACE := Gdrop
GIRNSVERSION := 0.1
IDPREFIX := Gdrop
GIRFILE := $(GIRNAMESPACE)-$(GIRNSVERSION).gir
GIRTYPELIBFILE=$(GIRFILE:%.gir=%.typelib)

ALL: $(BINARY) $(LIBRARY) $(GIRFILE) $(GIRTYPELIBFILE)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c -o $@ $<

$(BINARY): $(BINARYOBJECTS) $(HEADERS)
	$(CC) $(LIBS) -o $(BINARY) $(BINARYOBJECTS)

$(LIBRARY): $(LIBRARYOBJECTS) $(HEADERS)
	$(CC) $(LIBS) -shared -o $(LIBRARY) $(LIBRARYOBJECTS)

$(GIRFILE): $(SOURCES) $(HEADERS)
	$(GIRSCANNER) \
		--include=GObject-2.0 \
		--include=Gio-2.0 \
		--identifier-prefix=$(IDPREFIX) \
		--namespace=$(GIRNAMESPACE) \
		--nsversion=$(GIRNSVERSION) \
		--no-libtool \
		--library=drop \
		--output=$(GIRFILE) \
		$(GIRPKGS) \
		$(SOURCES) $(HEADERS)

$(GIRTYPELIBFILE): $(GIRFILE)
	$(GIRCOMPILER) --output=$(GIRTYPELIBFILE) $(GIRFILE)

clean:
	rm -fr $(BINARYOBJECTS) $(BINARY) $(LIBRARY) $(GIRFILE) $(GIRTYPELIBFILE)

.PHONY:
	clean