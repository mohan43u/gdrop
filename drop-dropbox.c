#include <drop.h>

static gchar* genremotepath(gchar *remote_root, gchar *remote_file)
{
  gchar *result = NULL;

  if(g_strcmp0(remote_file, "/") == 0)
    result = g_strdup(remote_root);
  else if(remote_file[0] == '/')
    result = g_strdup_printf("%s%s", remote_root, remote_file);
  else
    result = g_strdup_printf("%s/%s", remote_root, remote_file);

  return result;
}

gchar* drop_dropbox_accountinfo(gchar *consumer_key,
				gchar *consumer_secret,
				gchar *access_key,
				gchar *access_secret,
				gchar *locale,
				GError **error)
{
  gchar *url = NULL;
  gchar *geturl = NULL;
  GString *getargs = NULL;
  gchar *result = NULL;

  getargs = g_string_new(NULL);
  if(locale && strlen(locale))
    g_string_append_printf(getargs, "&locale=%s", locale);
  if(getargs->len)
    geturl = g_strdup_printf("%s?%s", D_R_ACCOUNTINFO, &(getargs->str[1]));
  else
    geturl = g_strdup(D_R_ACCOUNTINFO);
  g_string_free(getargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			geturl,
			NULL,
			"GET");
  g_free(geturl);
  result = drop_soup_sync(url, NULL, DROP_SOUP_OAUTH, error);
  g_free(url);
  return result;
}

gchar* drop_dropbox_put(gchar *consumer_key,
			gchar *consumer_secret,
			gchar *access_key,
			gchar *access_secret,
			gchar *remote_root,
			gchar *remote_file,
			gchar *filename,
			gchar *content_type,
			gchar *locale,
			gchar *overwrite,
			gchar *parent_rev,
			GError **error)
{
  gchar *url = NULL;
  gchar *puturl = NULL;
  GString *putargs = NULL;
  gchar *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  putargs = g_string_new(NULL);
  if(locale && strlen(locale))
    g_string_append_printf(putargs, "&locale=%s", locale);
  if(overwrite && strlen(overwrite))
    g_string_append_printf(putargs, "&overwrite=%s", overwrite);
  if(parent_rev && strlen(parent_rev))
    g_string_append_printf(putargs, "&parent_rev=%s", parent_rev);
  if(putargs->len)
    puturl = g_strdup_printf("%s/%s?%s",
			     D_R_PUT,
			     remotepath,
			     &(putargs->str[1]));
  else
    puturl = g_strdup_printf("%s/%s",
			     D_R_PUT,
			     remotepath);
  g_string_free(putargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			puturl,
			NULL,
			"PUT");
  g_free(puturl);
  result = drop_soup_put(url,
			 filename,
			 NULL,
			 0,
			 content_type,
			 DROP_SOUP_OAUTH,
			 error);
  g_free(remotepath);
  g_free(url);
  return result;
}

GByteArray* drop_dropbox_get(gchar *consumer_key,
			     gchar *consumer_secret,
			     gchar *access_key,
			     gchar *access_secret,
			     gchar *remote_root,
			     gchar *remote_file,
			     gchar *byterange,
			     gchar *rev,
			     gchar **metadata,
			     GError **error)
{
  gchar *url = NULL;
  gchar *signedurl = NULL;
  gchar *geturl = NULL;
  GString *getargs = NULL;
  GByteArray *result = NULL;
  guint flags = DROP_SOUP_OAUTH;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  getargs = g_string_new(NULL);
  if(rev && strlen(rev))
    g_string_append_printf(getargs, "&rev=%s", rev);
  if(getargs->len)
    geturl = g_strdup_printf("%s/%s?%s",
			     D_R_GET,
			     remotepath,
			     &(getargs->str[1]));
  else
    geturl = g_strdup_printf("%s/%s",
			     D_R_GET,
			     remotepath);
  g_string_free(getargs, TRUE);

  signedurl = drop_oauth_sign(consumer_key,
			      consumer_secret,
			      access_key,
			      access_secret,
			      geturl,
			      NULL,
			      "GET");
  g_free(geturl);

  if(byterange)
    {
      url = g_strdup_printf("%s&bytes=%s", signedurl, byterange);
      flags = flags | DROP_SOUP_RANGE;
    }
  else
    url = g_strdup(signedurl);
  g_free(signedurl);

  result = drop_soup_sync_bytes(url,
				NULL,
				flags,
				"x-dropbox-metadata",
				metadata,
				error);
  g_free(remotepath);
  g_free(url);
  return result;
}

gchar* drop_dropbox_metadata(gchar *consumer_key,
			     gchar *consumer_secret,
			     gchar *access_key,
			     gchar *access_secret,
			     gchar *remote_root,
			     gchar *remote_file,
			     gchar *file_limit,
			     gchar *hash,
			     gchar *list,
			     gchar *include_deleted,
			     gchar *rev,
			     gchar *locale,
			     GError **error)
{
  gchar *url = NULL;
  gchar *geturl = NULL;
  GString *getargs = NULL;
  gchar *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  getargs = g_string_new(NULL);
  if(file_limit && strlen(file_limit))
    g_string_append_printf(getargs, "&file_limit=%s", file_limit);
  if(hash && strlen(hash))
    g_string_append_printf(getargs, "&hash=%s", hash);
  if(list && strlen(list))
    g_string_append_printf(getargs, "&list=%s", list);
  if(include_deleted && strlen(include_deleted))
    g_string_append_printf(getargs, "&include_deleted=%s", include_deleted);
  if(rev && strlen(rev))
    g_string_append_printf(getargs, "&rev=%s", rev);
  if(locale && strlen(locale))
    g_string_append_printf(getargs, "&locale=%s", locale);
  if(getargs->len)
    geturl = g_strdup_printf("%s/%s?%s",
			     D_R_METADATA,
			     remotepath,
			     &(getargs->str[1]));
  else
    geturl = g_strdup_printf("%s/%s",
			     D_R_METADATA,
			     remotepath);
  g_string_free(getargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			geturl,
			NULL,
			"GET");
  g_free(geturl);
  result = drop_soup_sync(url, NULL, DROP_SOUP_OAUTH, error);
  g_free(remotepath);
  g_free(url);
  return result;
}

gchar* drop_dropbox_delta(gchar *consumer_key,
			  gchar *consumer_secret,
			  gchar *access_key,
			  gchar *access_secret,
			  gchar *cursor,
			  gchar *locale,
			  GError **error)
{
  gchar *url = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;

  postargs = g_string_new(NULL);
  if(cursor && strlen(cursor))
    g_string_append_printf(postargs, "&cursor=%s", cursor);
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(postargs->len)
    postparams = g_strdup(&(postargs->str[1]));
  else
    postparams = g_strdup("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			D_R_DELTA,
			&postparams,
			"POST");
  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(url);
  return result;
}

gchar* drop_dropbox_revisions(gchar *consumer_key,
			      gchar *consumer_secret,
			      gchar *access_key,
			      gchar *access_secret,
			      gchar *remote_root,
			      gchar *remote_file,
			      gchar *rev_limit,
			      gchar *locale,
			      GError **error)
{
  gchar *url = NULL;
  gchar *geturl = NULL;
  GString *getargs = NULL;
  gchar *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  getargs = g_string_new(NULL);
  if(rev_limit && strlen(rev_limit))
    g_string_append_printf(getargs, "&rev_limit=%s", rev_limit);
  if(locale && strlen(locale))
    g_string_append_printf(getargs, "&locale=%s", locale);
  if(getargs->len)
    geturl = g_strdup_printf("%s/%s?%s",
			     D_R_REVISIONS,
			     remotepath,
			     &(getargs->str[1]));
  else
    geturl = g_strdup_printf("%s/%s",
			     D_R_REVISIONS,
			     remotepath);
  g_string_free(getargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			geturl,
			NULL,
			"GET");
  g_free(geturl);
  result = drop_soup_sync(url, NULL, DROP_SOUP_OAUTH, error);
  g_free(remotepath);
  g_free(url);
  return result;
}

gchar* drop_dropbox_restore(gchar *consumer_key,
			    gchar *consumer_secret,
			    gchar *access_key,
			    gchar *access_secret,
			    gchar *remote_root,
			    gchar *remote_file,
			    gchar *rev,
			    gchar *locale,
			    GError **error)
{
  gchar *url = NULL;
  gchar *posturl = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  posturl = g_strdup_printf("%s/%s", D_R_RESTORE, remotepath);
  postargs = g_string_new(NULL);
  if(rev && strlen(rev))
    g_string_append_printf(postargs, "&rev=%s", rev);
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(postargs->len)
    postparams = g_strdup_printf("%s", &(postargs->str[1]));
  else
    postparams = g_strdup_printf("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			posturl,
			&postparams,
			"POST");
  g_free(posturl);
  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(remotepath);
  g_free(url);
  return result;
}

gchar* drop_dropbox_search(gchar *consumer_key,
			   gchar *consumer_secret,
			   gchar *access_key,
			   gchar *access_secret,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *query,
			   gchar *file_limit,
			   gchar *include_deleted,
			   gchar *locale,
			   GError **error)
{
  gchar *url = NULL;
  gchar *posturl = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  posturl = g_strdup_printf("%s/%s", D_R_SEARCH, remotepath);
  postargs = g_string_new(NULL);
  if(query && strlen(query))
    g_string_append_printf(postargs, "&query=%s", query);
  if(file_limit && strlen(file_limit))
    g_string_append_printf(postargs, "&file_limit=%s", file_limit);
  if(include_deleted && strlen(include_deleted))
    g_string_append_printf(postargs, "&include_deleted=%s", include_deleted);
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(postargs->len)
    postparams = g_strdup_printf("%s", &(postargs->str[1]));
  else
    postparams = g_strdup_printf("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			posturl,
			&postparams,
			"POST");
  g_free(posturl);
  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(remotepath);
  g_free(url);
  return result;
}

gchar* drop_dropbox_shares(gchar *consumer_key,
			   gchar *consumer_secret,
			   gchar *access_key,
			   gchar *access_secret,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *locale,
			   gchar *short_url,
			   GError **error)
{
  gchar *url = NULL;
  gchar *posturl = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  posturl = g_strdup_printf("%s/%s", D_R_SHARES, remotepath);
  postargs = g_string_new(NULL);
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(short_url && strlen(short_url))
    g_string_append_printf(postargs, "&short_url=%s", short_url);
  if(postargs->len)
    postparams = g_strdup_printf("%s", &(postargs->str[1]));
  else
    postparams = g_strdup_printf("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			posturl,
			&postparams,
			"POST");
  g_free(posturl);
  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(remotepath);
  g_free(url);
  return result;
}

gchar* drop_dropbox_media(gchar *consumer_key,
			  gchar *consumer_secret,
			  gchar *access_key,
			  gchar *access_secret,
			  gchar *remote_root,
			  gchar *remote_file,
			  gchar *locale,
			  GError **error)
{
  gchar *url = NULL;
  gchar *posturl = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  posturl = g_strdup_printf("%s/%s", D_R_MEDIA, remotepath);
  postargs = g_string_new(NULL);
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(postargs->len)
    postparams = g_strdup_printf("%s", &(postargs->str[1]));
  else
    postparams = g_strdup_printf("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			posturl,
			&postparams,
			"POST");
  g_free(posturl);
  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(remotepath);
  g_free(url);
  return result;
}

gchar* drop_dropbox_copy_ref(gchar *consumer_key,
			     gchar *consumer_secret,
			     gchar *access_key,
			     gchar *access_secret,
			     gchar *remote_root,
			     gchar *remote_file,
			     GError **error)
{
  gchar *url = NULL;
  gchar *geturl = NULL;
  gchar *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  geturl = g_strdup_printf("%s/%s",
			   D_R_COPY_REF,
			   remotepath);
  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			geturl,
			NULL,
			"GET");
  g_free(geturl);
  result = drop_soup_sync(url, NULL, DROP_SOUP_OAUTH, error);
  g_free(remotepath);
  g_free(url);
  return result;
}

GByteArray* drop_dropbox_thumbnails(gchar *consumer_key,
				    gchar *consumer_secret,
				    gchar *access_key,
				    gchar *access_secret,
				    gchar *remote_root,
				    gchar *remote_file,
				    gchar *format,
				    gchar *size,
				    gchar **metadata,
				    GError **error)
{
  gchar *url = NULL;
  gchar *geturl = NULL;
  GString *getargs = NULL;
  GByteArray *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  getargs = g_string_new(NULL);
  if(format && strlen(format))
    g_string_append_printf(getargs, "&format=%s", format);
  if(size && strlen(size))
    g_string_append_printf(getargs, "&size=%s", size);
  if(getargs->len)
    geturl = g_strdup_printf("%s/%s?%s",
			     D_R_THUMBNAILS,
			     remotepath,
			     &(getargs->str[1]));
  else
    geturl = g_strdup_printf("%s/%s",
			     D_R_THUMBNAILS,
			     remotepath);
  g_string_free(getargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			geturl,
			NULL,
			"GET");
  g_free(geturl);
  result = drop_soup_sync_bytes(url,
				NULL,
				DROP_SOUP_OAUTH,
				"x-dropbox-metadata",
				metadata,
				error);
  g_free(remotepath);
  g_free(url);
  return result;
}

gchar* drop_dropbox_chunked_upload(gchar *consumer_key,
				   gchar *consumer_secret,
				   gchar *access_key,
				   gchar *access_secret,
				   gpointer chunk,
				   gsize chunklength,
				   gchar *content_type,
				   gchar *upload_id,
				   gchar *offset,
				   GError **error)
{
  gchar *url = NULL;
  gchar *puturl = NULL;
  GString *putargs = NULL;
  gchar *result = NULL;

  putargs = g_string_new(NULL);
  if(upload_id && strlen(upload_id))
    g_string_append_printf(putargs, "&upload_id=%s", upload_id);
  if(offset && strlen(offset))
    g_string_append_printf(putargs, "&offset=%s", offset);
  if(putargs->len)
    puturl = g_strdup_printf("%s?%s",
			     D_R_CHUNKED_UPLOAD,
			     &(putargs->str[1]));
  else
    puturl = g_strdup_printf("%s",
			     D_R_CHUNKED_UPLOAD);
  g_string_free(putargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			puturl,
			NULL,
			"PUT");
  g_free(puturl);
  result = drop_soup_put(url,
			 NULL,
			 chunk,
			 chunklength,
			 content_type,
			 DROP_SOUP_OAUTH,
			 error);
  g_free(url);
  return result;
}

gchar* drop_dropbox_commit_chunked_upload(gchar *consumer_key,
					  gchar *consumer_secret,
					  gchar *access_key,
					  gchar *access_secret,
					  gchar *remote_root,
					  gchar *remote_file,
					  gchar *locale,
					  gchar *overwrite,
					  gchar *parent_rev,
					  gchar *upload_id,
					  GError **error)
{
  gchar *url = NULL;
  gchar *posturl = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;
  gchar *remotepath = NULL;

  remotepath = genremotepath(remote_root, remote_file);
  posturl = g_strdup_printf("%s/%s", D_R_COMMIT_CHUNKED_UPLOAD, remotepath);
  postargs = g_string_new(NULL);
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(overwrite && strlen(overwrite))
    g_string_append_printf(postargs, "&overwrite=%s", overwrite);
  if(parent_rev && strlen(parent_rev))
    g_string_append_printf(postargs, "&parent_rev=%s", parent_rev);
  if(upload_id && strlen(upload_id))
    g_string_append_printf(postargs, "&upload_id=%s", upload_id);
  if(postargs->len)
    postparams = g_strdup_printf("%s", &(postargs->str[1]));
  else
    postparams = g_strdup_printf("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			posturl,
			&postparams,
			"POST");
  g_free(posturl);
  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(remotepath);
  g_free(url);
  return result;
}

gchar* drop_dropbox_copy(gchar *consumer_key,
			 gchar *consumer_secret,
			 gchar *access_key,
			 gchar *access_secret,
			 gchar *remote_root,
			 gchar *from_path,
			 gchar *to_path,
			 gchar *locale,
			 gchar *from_copy_ref,
			 GError **error)
{
  gchar *url = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;
  gchar *remotefrom = NULL;
  gchar *remoteto = NULL;

  postargs = g_string_new(NULL);
  if(remote_root && strlen(remote_root))
    g_string_append_printf(postargs, "&root=%s", remote_root);
  if(from_path && strlen(from_path))
    g_string_append_printf(postargs, "&from_path=%s",
			   (from_path[0] == '/' ? &from_path[1] : from_path));
  if(to_path && strlen(to_path))
    g_string_append_printf(postargs, "&to_path=%s",
			   (to_path[0] == '/' ? &to_path[1] : to_path));
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(from_copy_ref && strlen(from_copy_ref))
    g_string_append_printf(postargs, "&from_copy_ref=%s", from_copy_ref);
  if(postargs->len)
    postparams = g_strdup_printf("%s", &(postargs->str[1]));
  else
    postparams = g_strdup_printf("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			D_R_COPY,
			&postparams,
			"POST");

  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(url);
  return result;
}

gchar* drop_dropbox_create_folder(gchar *consumer_key,
				  gchar *consumer_secret,
				  gchar *access_key,
				  gchar *access_secret,
				  gchar *remote_root,
				  gchar *remote_path,
				  gchar *locale,
				  GError **error)
{
  gchar *url = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;
  gchar *remotefrom = NULL;
  gchar *remoteto = NULL;

  postargs = g_string_new(NULL);
  if(remote_root && strlen(remote_root))
    g_string_append_printf(postargs, "&root=%s", remote_root);
  if(remote_path && strlen(remote_path))
    g_string_append_printf(postargs, "&path=%s",
			   (remote_path[0] == '/' ? &remote_path[1] : remote_path));
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(postargs->len)
    postparams = g_strdup_printf("%s", &(postargs->str[1]));
  else
    postparams = g_strdup_printf("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			D_R_CREATE_FOLDER,
			&postparams,
			"POST");

  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(url);
  return result;
}

gchar* drop_dropbox_delete(gchar *consumer_key,
			   gchar *consumer_secret,
			   gchar *access_key,
			   gchar *access_secret,
			   gchar *remote_root,
			   gchar *remote_path,
			   gchar *locale,
			   GError **error)
{
  gchar *url = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;
  gchar *remotefrom = NULL;
  gchar *remoteto = NULL;

  postargs = g_string_new(NULL);
  if(remote_root && strlen(remote_root))
    g_string_append_printf(postargs, "&root=%s", remote_root);
  if(remote_path && strlen(remote_path))
    g_string_append_printf(postargs, "&path=%s",
			   (remote_path[0] == '/' ? &remote_path[1] : remote_path));
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(postargs->len)
    postparams = g_strdup_printf("%s", &(postargs->str[1]));
  else
    postparams = g_strdup_printf("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			D_R_DELETE,
			&postparams,
			"POST");

  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(url);
  return result;
}

gchar* drop_dropbox_move(gchar *consumer_key,
			 gchar *consumer_secret,
			 gchar *access_key,
			 gchar *access_secret,
			 gchar *remote_root,
			 gchar *from_path,
			 gchar *to_path,
			 gchar *locale,
			 GError **error)
{
  gchar *url = NULL;
  gchar *postparams = NULL;
  GString *postargs = NULL;
  gchar *result = NULL;
  gchar *remotefrom = NULL;
  gchar *remoteto = NULL;

  postargs = g_string_new(NULL);
  if(remote_root && strlen(remote_root))
    g_string_append_printf(postargs, "&root=%s", remote_root);
  if(from_path && strlen(from_path))
    g_string_append_printf(postargs, "&from_path=%s",
			   (from_path[0] == '/' ? &from_path[1] : from_path));
  if(to_path && strlen(to_path))
    g_string_append_printf(postargs, "&to_path=%s",
			   (to_path[0] == '/' ? &to_path[1] : to_path));
  if(locale && strlen(locale))
    g_string_append_printf(postargs, "&locale=%s", locale);
  if(postargs->len)
    postparams = g_strdup_printf("%s", &(postargs->str[1]));
  else
    postparams = g_strdup_printf("");
  g_string_free(postargs, TRUE);

  url = drop_oauth_sign(consumer_key,
			consumer_secret,
			access_key,
			access_secret,
			D_R_MOVE,
			&postparams,
			"POST");

  result = drop_soup_sync(url, postparams, DROP_SOUP_OAUTH, error);
  g_free(postparams);
  g_free(url);
  return result;
}
