#ifndef __DROP_DROPBOX_H__
#define __DROP_DROPBOX_H__

#define D_R_ACCOUNTINFO "https://api.dropbox.com/1/account/info"
#define D_R_PUT "https://api-content.dropbox.com/1/files_put"
#define D_R_GET "https://api-content.dropbox.com/1/files"
#define D_R_METADATA "https://api.dropbox.com/1/metadata"
#define D_R_DELTA "https://api.dropbox.com/1/delta"
#define D_R_REVISIONS "https://api.dropbox.com/1/revisions"
#define D_R_RESTORE "https://api.dropbox.com/1/restore"
#define D_R_SEARCH "https://api.dropbox.com/1/search"
#define D_R_SHARES "https://api.dropbox.com/1/shares"
#define D_R_MEDIA "https://api.dropbox.com/1/media"
#define D_R_COPY_REF "https://api.dropbox.com/1/copy_ref"
#define D_R_THUMBNAILS "https://api-content.dropbox.com/1/thumbnails"
#define D_R_CHUNKED_UPLOAD "https://api-content.dropbox.com/1/chunked_upload"
#define D_R_COMMIT_CHUNKED_UPLOAD "https://api-content.dropbox.com/1/commit_chunked_upload"
#define D_R_COPY "https://api.dropbox.com/1/fileops/copy"
#define D_R_CREATE_FOLDER "https://api.dropbox.com/1/fileops/create_folder"
#define D_R_DELETE "https://api.dropbox.com/1/fileops/delete"
#define D_R_MOVE "https://api.dropbox.com/1/fileops/move"

gchar* drop_dropbox_accountinfo(gchar *consumer_key,
				gchar *consumer_secret,
				gchar *access_key,
				gchar *access_secret,
				gchar *locale,
				GError **error);
gchar* drop_dropbox_put(gchar *consumer_key,
			gchar *consumer_secret,
			gchar *access_key,
			gchar *access_secret,
			gchar *remote_root,
			gchar *remote_file,
			gchar *filename,
			gchar *content_type,
			gchar *locale,
			gchar *overwrite,
			gchar *parent_rev,
			GError **error);
GByteArray* drop_dropbox_get(gchar *consumer_key,
			     gchar *consumer_secret,
			     gchar *access_key,
			     gchar *access_secret,
			     gchar *remote_root,
			     gchar *remote_file,
			     gchar *byterange,
			     gchar *rev,
			     gchar **metadata,
			     GError **error);
gchar* drop_dropbox_metadata(gchar *consumer_key,
			     gchar *consumer_secret,
			     gchar *access_key,
			     gchar *access_secret,
			     gchar *remote_root,
			     gchar *remote_file,
			     gchar *file_limit,
			     gchar *hash,
			     gchar *list,
			     gchar *include_deleted,
			     gchar *rev,
			     gchar *locale,
			     GError **error);
gchar* drop_dropbox_delta(gchar *consumer_key,
			  gchar *consumer_secret,
			  gchar *access_key,
			  gchar *access_secret,
			  gchar *cursor,
			  gchar *locale,
			  GError **error);
gchar* drop_dropbox_revisions(gchar *consumer_key,
			      gchar *consumer_secret,
			      gchar *access_key,
			      gchar *access_secret,
			      gchar *remote_root,
			      gchar *remote_file,
			      gchar *rev_limit,
			      gchar *locale,
			      GError **error);
gchar* drop_dropbox_restore(gchar *consumer_key,
			    gchar *consumer_secret,
			    gchar *access_key,
			    gchar *access_secret,
			    gchar *remote_root,
			    gchar *remote_file,
			    gchar *rev,
			    gchar *locale,
			    GError **error);
gchar* drop_dropbox_search(gchar *consumer_key,
			   gchar *consumer_secret,
			   gchar *access_key,
			   gchar *access_secret,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *query,
			   gchar *file_limit,
			   gchar *include_deleted,
			   gchar *locale,
			   GError **error);
gchar* drop_dropbox_shares(gchar *consumer_key,
			   gchar *consumer_secret,
			   gchar *access_key,
			   gchar *access_secret,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *locale,
			   gchar *short_url,
			   GError **error);
gchar* drop_dropbox_media(gchar *consumer_key,
			  gchar *consumer_secret,
			  gchar *access_key,
			  gchar *access_secret,
			  gchar *remote_root,
			  gchar *remote_file,
			  gchar *locale,
			  GError **error);
gchar* drop_dropbox_copy_ref(gchar *consumer_key,
			     gchar *consumer_secret,
			     gchar *access_key,
			     gchar *access_secret,
			     gchar *remote_root,
			     gchar *remote_file,
			     GError **error);
GByteArray* drop_dropbox_thumbnails(gchar *consumer_key,
				    gchar *consumer_secret,
				    gchar *access_key,
				    gchar *access_secret,
				    gchar *remote_root,
				    gchar *remote_file,
				    gchar *format,
				    gchar *size,
				    gchar **metadata,
				    GError **error);
gchar* drop_dropbox_chunked_upload(gchar *consumer_key,
				   gchar *consumer_secret,
				   gchar *access_key,
				   gchar *access_secret,
				   gpointer chunk,
				   gsize chunklength,
				   gchar *content_type,
				   gchar *upload_id,
				   gchar *offset,
				   GError **error);
gchar* drop_dropbox_commit_chunked_upload(gchar *consumer_key,
					  gchar *consumer_secret,
					  gchar *access_key,
					  gchar *access_secret,
					  gchar *remote_root,
					  gchar *remote_file,
					  gchar *locale,
					  gchar *overwrite,
					  gchar *parent_rev,
					  gchar *upload_id,
					  GError **error);
gchar* drop_dropbox_copy(gchar *consumer_key,
			 gchar *consumer_secret,
			 gchar *access_key,
			 gchar *access_secret,
			 gchar *remote_root,
			 gchar *from_path,
			 gchar *to_path,
			 gchar *locale,
			 gchar *from_copy_ref,
			 GError **error);
gchar* drop_dropbox_create_folder(gchar *consumer_key,
				  gchar *consumer_secret,
				  gchar *access_key,
				  gchar *access_secret,
				  gchar *remote_root,
				  gchar *remote_path,
				  gchar *locale,
				  GError **error);
gchar* drop_dropbox_delete(gchar *consumer_key,
			   gchar *consumer_secret,
			   gchar *access_key,
			   gchar *access_secret,
			   gchar *remote_root,
			   gchar *remote_path,
			   gchar *locale,
			   GError **error);
gchar* drop_dropbox_move(gchar *consumer_key,
			 gchar *consumer_secret,
			 gchar *access_key,
			 gchar *access_secret,
			 gchar *remote_root,
			 gchar *from_path,
			 gchar *to_path,
			 gchar *locale,
			 GError **error);

#endif /* __DROP_DROPBOX_H__ */
