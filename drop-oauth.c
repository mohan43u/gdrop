#include <drop.h>

gchar* drop_oauth_expandfilename(gchar *filename)
{
  wordexp_t result_t;
  gchar *result = NULL;
  if(wordexp(filename, &result_t, 0) == 0)
    result = g_strjoinv(" ", result_t.we_wordv);
  else
    result = g_strdup(filename);
  wordfree(&result_t);
  return result;
}

gchar* drop_oauth_sign(gchar *consumer_key,
		       gchar *consumer_secret,
		       gchar *access_key,
		       gchar *access_secret,
		       gchar *url,
		       gchar **params,
		       gchar *method)
{
  gchar *requesturl = NULL;
  gchar *resulturl = NULL;

  if(params)
    requesturl = g_strdup_printf("%s?%s", url, *params);
  else
    requesturl = g_strdup(url);

  resulturl = oauth_sign_url2(requesturl,
			      params,
			      OA_HMAC,
			      method,
			      consumer_key,
			      consumer_secret,
			      access_key,
			      access_secret);

  g_free(requesturl);
  return resulturl;
}

void drop_oauth_request_token(gchar *consumer_key,
			      gchar *consumer_secret,
			      gchar **request_key,
			      gchar **request_secret,
			      GError **error)
{
  gchar *request = NULL;
  gchar *response = NULL;
  gchar **token = NULL;
  gint argc = 0;
  gchar **argv = NULL;
  gchar *postarg = NULL;

  request = g_strdup_printf("%s", REQ_TOKEN_URL);
  oauth_sign_url2(request,
		  &postarg,
		  OA_HMAC,
		  NULL,
		  consumer_key,
		  consumer_secret,
		  NULL,
		  NULL);

  response = drop_soup_sync(REQ_TOKEN_URL, postarg, 0, error);
  argc = oauth_split_post_paramters(response, &argv, 0);

  while(argc)
    {
      token = g_strsplit(argv[argc - 1], "=", 0);
      if(g_strcmp0(token[0], "oauth_token") == 0)
	*request_key = g_strdup(token[1]);
      if(g_strcmp0(token[0], "oauth_token_secret") == 0)
	*request_secret = g_strdup(token[1]);
      argc--;
    }

  g_free(request);
  g_free(postarg);
  g_free(response);
  g_strfreev(token);
  oauth_free_array(&argc, &argv);
}

gchar* drop_oauth_gen_authurl(gchar *consumer_key,
			      gchar *consumer_secret,
			      gchar *request_key,
			      gchar *request_secret)
{
  gchar *authurl = NULL;
  gchar *request = NULL;

  request = oauth_sign_url2(AUTH_URL,
			    NULL,
			    OA_HMAC,
			    NULL,
			    consumer_key,
			    consumer_secret,
			    request_key,
			    request_secret);
  authurl = g_strdup_printf("%s&"
			    "oauth_token=%s",
			    request,
			    request_key);

  g_free(request);
  return authurl;
}

void drop_oauth_access_token(gchar *consumer_key,
			     gchar *consumer_secret,
			     gchar *request_key,
			     gchar *request_secret,
			     gchar **access_key,
			     gchar **access_secret,
			     GError **error)
{
  gchar *response = NULL;
  gint argc = 0;
  gchar **argv = NULL;
  gchar *postarg = NULL;
  gchar **token = NULL;

  oauth_sign_url2(ACCESS_TOKEN_URL,
		  &postarg,
		  OA_HMAC,
		  NULL,
		  consumer_key,
		  consumer_secret,
		  request_key,
		  request_secret);

  response = drop_soup_sync(ACCESS_TOKEN_URL, postarg, DROP_SOUP_OAUTH, error);
  argc = oauth_split_post_paramters(response, &argv, 0);

  while(argc)
    {
      token = g_strsplit(argv[argc - 1], "=", 0);
      if(g_strcmp0(token[0], "oauth_token") == 0)
	*access_key = g_strdup(token[1]);
      if(g_strcmp0(token[0], "oauth_token_secret") == 0)
	*access_secret = g_strdup(token[1]);
      argc--;
    }

  if(access_key == NULL || access_secret == NULL)
    {
      g_print("things went wrong!! not able to get access_{key,secret}!!\n");
      exit(EXIT_FAILURE);
    }

  g_free(postarg);
  g_free(response);
  g_strfreev(token);
  oauth_free_array(&argc, &argv);
}

void drop_oauth_to_file(gchar *appname,
			gchar *consumer_key,
			gchar *consumer_secret,
			gchar *access_key,
			gchar *access_secret)
{
  gchar *content = NULL;
  gchar *oauthfile = NULL;

  content = g_strdup_printf("appname=%s&"
			    "consumer_key=%s&consumer_secret=%s&"
			    "access_key=%s&access_secret=%s",
			    appname,
			    consumer_key,
			    consumer_secret,
			    access_key,
			    access_secret);

  oauthfile = drop_oauth_expandfilename(OAUTHFILE);
  g_file_set_contents(oauthfile,
		      content,
		      strlen(content),
		      NULL);

  g_free(content);
  g_free(oauthfile);
}

gboolean drop_oauth_from_file(gchar **appname,
			      gchar **consumer_key,
			      gchar **consumer_secret,
			      gchar **access_key,
			      gchar **access_secret)
{
  gchar *response = NULL;
  gint argc = 0;
  gchar **argv = NULL;
  gchar **pair = NULL;
  gboolean result = FALSE;
  gchar *oauthfile = NULL;

  oauthfile = drop_oauth_expandfilename(OAUTHFILE);

  if(g_file_get_contents(oauthfile, &response, NULL, NULL))
    {
      argc = oauth_split_post_paramters(response, &argv, 0);
      while(argc)
	{
	  pair = g_strsplit(argv[argc - 1], "=", 0);
	  if(g_strcmp0(pair[0], "appname") == 0)
	    *appname = g_strdup(pair[1]);
	  if(g_strcmp0(pair[0], "consumer_key") == 0)
	    *consumer_key = g_strdup(pair[1]);
	  if(g_strcmp0(pair[0], "consumer_secret") == 0)
	    *consumer_secret = g_strdup(pair[1]);
	  if(g_strcmp0(pair[0], "access_key") == 0)
	    *access_key = g_strdup(pair[1]);
	  if(g_strcmp0(pair[0], "access_secret") == 0)
	    *access_secret = g_strdup(pair[1]);
	  argc--;
	  g_strfreev(pair);
	}
      if(*appname &&
	 *consumer_key &&
	 *consumer_secret &&
	 *access_key &&
	 *access_secret)
	result = TRUE;

      oauth_free_array(&argc, &argv);
    }

  g_free(response);
  g_free(oauthfile);
  return result;
}
