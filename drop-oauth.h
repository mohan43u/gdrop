#ifndef __DROP_OAUTH_H__
#define __DROP_OAUTH_H__

#include <oauth.h>
#include <wordexp.h>

#define REQ_TOKEN_URL "https://api.dropbox.com/1/oauth/request_token"
#define AUTH_URL "https://www.dropbox.com/1/oauth/authorize"
#define ACCESS_TOKEN_URL "https://api.dropbox.com/1/oauth/access_token"
#define OAUTHFILE "~/.libgdrop"

gchar* drop_oauth_expandfilename(gchar *filename);
gchar* drop_oauth_sign(gchar *consumer_key,
		       gchar *consumer_secret,
		       gchar *access_key,
		       gchar *access_secret,
		       gchar *url,
		       gchar **params,
		       gchar *method);
void drop_oauth_request_token(gchar *consumer_key,
			      gchar *consumer_secret,
			      gchar **request_key,
			      gchar **request_secret,
			      GError **error);
gchar* drop_oauth_gen_authurl(gchar *consumer_key,
			      gchar *consumer_secret,
			      gchar *request_key,
			      gchar *request_secret);
void drop_oauth_access_token(gchar *consumer_key,
			     gchar *consumer_secret,
			     gchar *request_key,
			     gchar *request_secret,
			     gchar **access_key,
			     gchar **access_secret,
			     GError **error);
void drop_oauth_to_file(gchar *appname,
			gchar *consumer_key,
			gchar *consumer_secret,
			gchar *access_key,
			gchar *access_secret);
gboolean drop_oauth_from_file(gchar **appname,
			      gchar **consumer_key,
			      gchar **consumer_secret,
			      gchar **access_key,
			      gchar **access_secret);

#endif /* __DROP_OAUTH_H__ */
