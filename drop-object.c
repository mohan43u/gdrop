#include <drop.h>

static void gdrop_object_class_init(GdropObjectClass *klass);
static void gdrop_object_init(GdropObject *object){}
G_DEFINE_TYPE(GdropObject, gdrop_object, G_TYPE_OBJECT);

static GObject* gdrop_object_constructor(GType type,
					 guint n_construct_properties,
					 GObjectConstructParam *construct_properties)
{
  GObject *object = NULL;

  object = G_OBJECT_CLASS(gdrop_object_parent_class)->constructor(type,
								  n_construct_properties,
								  construct_properties);
  drop_soup_init();

  return object;
}

static void gdrop_object_finalize(GObject *object)
{
  GdropObject *obj = NULL;

  obj = GDROP_OBJECT(object);
  g_return_if_fail(GDROP_IS_OBJECT(object));
  if(G_OBJECT_CLASS(gdrop_object_parent_class)->finalize)
    G_OBJECT_CLASS(gdrop_object_parent_class)->finalize(object);

  drop_soup_free();
}

static void gdrop_object_set_property(GObject *object,
				      guint property_id,
				      const GValue *value,
				      GParamSpec *pspec)
{
  GdropObject *dropObject = GDROP_OBJECT(object);

  g_return_if_fail(GDROP_IS_OBJECT(object));

  switch(property_id)
    {
    case APPNAME:
      dropObject->appname = g_value_dup_string(value);
      break;
    case CONSUMER_KEY:
      dropObject->consumer_key = g_value_dup_string(value);
      break;
    case CONSUMER_SECRET:
      dropObject->consumer_secret = g_value_dup_string(value);
      break;
    case REQUEST_KEY:
      dropObject->request_key = g_value_dup_string(value);
      break;
    case REQUEST_SECRET:
      dropObject->request_secret = g_value_dup_string(value);
      break;
    case ACCESS_KEY:
      dropObject->access_key = g_value_dup_string(value);
      break;
    case ACCESS_SECRET:
      dropObject->access_secret = g_value_dup_string(value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    }
}

static void gdrop_object_get_property(GObject *object,
				      guint property_id,
				      GValue *value,
				      GParamSpec *pspec)
{
  GdropObject *dropObject = GDROP_OBJECT(object);

  g_return_if_fail(GDROP_IS_OBJECT(object));

  switch(property_id)
    {
    case APPNAME:
      g_value_set_string(value, dropObject->appname);
      break;
    case CONSUMER_KEY:
      g_value_set_string(value, dropObject->consumer_key);
      break;
    case CONSUMER_SECRET:
      g_value_set_string(value, dropObject->consumer_secret);
      break;
    case REQUEST_KEY:
      g_value_set_string(value, dropObject->request_key);
      break;
    case REQUEST_SECRET:
      g_value_set_string(value, dropObject->request_secret);
      break;
    case ACCESS_KEY:
      g_value_set_string(value, dropObject->access_key);
      break;
    case ACCESS_SECRET:
      g_value_set_string(value, dropObject->access_secret);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    }
}

static void gdrop_object_class_init(GdropObjectClass *klass)
{
  GObjectClass *gobjectClass = G_OBJECT_CLASS(klass);
  gobjectClass->constructor = gdrop_object_constructor;
  gobjectClass->finalize = gdrop_object_finalize;
  gobjectClass->set_property = gdrop_object_set_property;
  gobjectClass->get_property = gdrop_object_get_property;

  g_object_class_install_property(gobjectClass,
				  APPNAME,
				  g_param_spec_string("appname",
						      "application name",
						      "will be stored for reference",
						      NULL,
						      G_PARAM_READABLE|G_PARAM_WRITABLE));
  g_object_class_install_property(gobjectClass,
				  CONSUMER_KEY,
				  g_param_spec_string("consumer_key",
						      "dropbox consumer key",
						      "will be used to generate authurl",
						      NULL,
						      G_PARAM_READABLE|G_PARAM_WRITABLE));
  g_object_class_install_property(gobjectClass,
				  CONSUMER_SECRET,
				  g_param_spec_string("consumer_secret",
						      "dropbox consumer secret",
						      "will be used to generate authurl",
						      NULL,
						      G_PARAM_READABLE|G_PARAM_WRITABLE));
  g_object_class_install_property(gobjectClass,
				  REQUEST_KEY,
				  g_param_spec_string("request_key",
						      "dropbox request key",
						      "will be used to generate authurl",
						      NULL,
						      G_PARAM_READABLE|G_PARAM_WRITABLE));
  g_object_class_install_property(gobjectClass,
				  REQUEST_SECRET,
				  g_param_spec_string("request_secret",
						      "dropbox request secret",
						      "will be used to generate authurl",
						      NULL,
						      G_PARAM_READABLE|G_PARAM_WRITABLE));
  g_object_class_install_property(gobjectClass,
				  ACCESS_KEY,
				  g_param_spec_string("access_key",
						      "dropbox access key",
						      "will be used to sign",
						      NULL,
						      G_PARAM_READABLE|G_PARAM_WRITABLE));
  g_object_class_install_property(gobjectClass,
				  ACCESS_SECRET,
				  g_param_spec_string("access_secret",
						      "dropbox access secret",
						      "will be used to sign",
						      NULL,
						      G_PARAM_READABLE|G_PARAM_WRITABLE));
}

GdropObject* gdrop_object_new(void)
{
  return g_object_new(GDROP_TYPE_OBJECT, NULL);
}

static void print_properties(GdropObject *dropObject)
{
  gchar *appname = NULL;
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *request_key = NULL;
  gchar *request_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;

  g_object_get(dropObject,
	       "appname", &appname,
	       "consumer_key", &consumer_key,
	       "consumer_secret", &consumer_secret,
	       "request_key", &request_key,
	       "request_secret", &request_secret,
	       "access_key", &access_key,
	       "access_secret", &access_secret,
	       NULL);

  g_printerr("appname = %s\n"
	     "consumer_key = %s\n"
	     "consumer_secret = %s\n"
	     "request_key = %s\n"
	     "request_secret = %s\n"
	     "access_key = %s\n"
	     "access_secret = %s\n",
	     appname,
	     consumer_key,
	     consumer_secret,
	     request_key,
	     request_secret,
	     access_key,
	     access_secret);

  g_free(appname);
  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(request_key);
  g_free(request_secret);
  g_free(access_key);
  g_free(access_secret);
}

gboolean gdrop_object_initkeys(GdropObject *dropObject)
{
  gchar *appname = NULL;
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gboolean result = FALSE;

  result = drop_oauth_from_file(&appname,
				&consumer_key,
				&consumer_secret,
				&access_key,
				&access_secret);
  if(result == TRUE)
    g_object_set(dropObject,
		 "appname", appname,
		 "consumer_key", consumer_key,
		 "consumer_secret", consumer_secret,
		 "access_key", access_key,
		 "access_secret", access_secret,
		 NULL);

  g_free(appname);
  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);
  return result;
}

gchar* gdrop_object_gen_authurl(GdropObject *dropObject,
				gchar *consumer_key,
				gchar *consumer_secret,
				GError **error)
{
  gchar *authurl = NULL;
  gchar *request_key = NULL;
  gchar *request_secret = NULL;

  drop_oauth_request_token(consumer_key,
			   consumer_secret,
			   &request_key,
			   &request_secret,
			   error);
  if(request_key && request_secret)
    {
      g_object_set(G_OBJECT(dropObject),
		   "consumer_key", consumer_key,
		   "consumer_secret", consumer_secret,
		   "request_key", request_key,
		   "request_secret", request_secret,
		   NULL);
      authurl = drop_oauth_gen_authurl(consumer_key,
				       consumer_secret,
				       request_key,
				       request_secret);
    }

  g_free(request_key);
  g_free(request_secret);
  return authurl;
}

gboolean gdrop_object_auth(GdropObject *dropObject,
			   gchar *appname,
			   GError **error)
{
  gboolean result = FALSE;
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *request_key = NULL;
  gchar *request_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;

  g_object_get(G_OBJECT(dropObject),
	       "consumer_key", &consumer_key,
	       "consumer_secret", &consumer_secret,
	       "request_key", &request_key,
	       "request_secret", &request_secret,
	       NULL);

  drop_oauth_access_token(consumer_key,
			  consumer_secret,
			  request_key,
			  request_secret,
			  &access_key,
			  &access_secret,
			  error);

  if(access_key && access_secret)
    {
      g_object_set(G_OBJECT(dropObject),
		   "appname", appname,
		   "access_key", access_key,
		   "access_secret", access_secret,
		   NULL);
      drop_oauth_to_file(appname,
			 consumer_key,
			 consumer_secret,
			 access_key,
			 access_secret);
      result = TRUE;
    }

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(request_key);
  g_free(request_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

gchar* gdrop_object_accountinfo(GdropObject *dropObject,
				gchar *locale,
				GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_accountinfo(consumer_key,
				    consumer_secret,
				    access_key,
				    access_secret,
				    locale,
				    error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

gchar* gdrop_object_put(GdropObject *dropObject,
			gchar *remote_root,
			gchar *remote_file,
			gchar *filename,
			gchar *content_type,
			gchar *locale,
			gchar *overwrite,
			gchar *parent_rev,
			GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_put(consumer_key,
			    consumer_secret,
			    access_key,
			    access_secret,
			    remote_root,
			    remote_file,
			    filename,
			    content_type,
			    locale,
			    overwrite,
			    parent_rev,
			    error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

static void gdrop_object_put_async_threadfunc(GSimpleAsyncResult *res,
					      GObject *object,
					      GCancellable *cancellable)
{
  GdropObject *dropObject = GDROP_OBJECT(object);
  GPtrArray *args = g_simple_async_result_get_op_res_gpointer(res);
  gchar *remote_root = g_ptr_array_index(args, 0);
  gchar *remote_file = g_ptr_array_index(args, 1);
  gchar *filename = g_ptr_array_index(args, 2);
  gchar *content_type = g_ptr_array_index(args, 3);
  gchar *locale = g_ptr_array_index(args, 4);
  gchar *overwrite = g_ptr_array_index(args, 5);
  gchar *parent_rev = g_ptr_array_index(args, 6);
  gchar *result = NULL;
  GError *error = NULL;

  result = gdrop_object_put(dropObject,
			    remote_root,
			    remote_file,
			    filename,
			    content_type,
			    locale,
			    overwrite,
			    parent_rev,
			    &error);
  if(error) g_simple_async_result_take_error(res, error);
  g_simple_async_result_set_op_res_gpointer(res, result, NULL);
  g_ptr_array_free(args, TRUE);
}

void gdrop_object_put_async(GdropObject *dropObject,
			    gchar *remote_root,
			    gchar *remote_file,
			    gchar *filename,
			    gchar *content_type,
			    gchar *locale,
			    gchar *overwrite,
			    gchar *parent_rev,
			    GCancellable *cancellable,
			    GAsyncReadyCallback callback,
			    gpointer user_data)
{
  GSimpleAsyncResult *simpleAsyncResult = NULL;
  GPtrArray *args = NULL;

  simpleAsyncResult = g_simple_async_result_new(G_OBJECT(dropObject),
						callback,
						user_data,
						gdrop_object_put_async);
  g_simple_async_result_set_check_cancellable(simpleAsyncResult,
					      cancellable);

  args = g_ptr_array_new();
  g_ptr_array_add(args, g_strdup(remote_root));
  g_ptr_array_add(args, g_strdup(remote_file));
  g_ptr_array_add(args, g_strdup(filename));
  g_ptr_array_add(args, g_strdup(content_type));
  g_ptr_array_add(args, g_strdup(locale));
  g_ptr_array_add(args, g_strdup(overwrite));
  g_ptr_array_add(args, g_strdup(parent_rev));
  g_simple_async_result_set_op_res_gpointer(simpleAsyncResult,
					    args,
					    NULL);
  g_simple_async_result_run_in_thread(simpleAsyncResult,
				      gdrop_object_put_async_threadfunc,
				      G_PRIORITY_DEFAULT,
				      cancellable);

  g_object_unref(simpleAsyncResult);
}

gchar* gdrop_object_put_finish(GdropObject *dropObject,
			       GAsyncResult *result,
			       GError **error)
{
  GSimpleAsyncResult *asyncResult = G_SIMPLE_ASYNC_RESULT(result);

  if(g_simple_async_result_propagate_error(asyncResult, error))
    return NULL;
  else
    return g_simple_async_result_get_op_res_gpointer(asyncResult);
}

GByteArray* gdrop_object_get(GdropObject *dropObject,
			     gchar *remote_root,
			     gchar *remote_file,
			     gchar *byterange,
			     gchar *rev,
			     gchar **metadata,
			     GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  GByteArray *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_get(consumer_key,
			    consumer_secret,
			    access_key,
			    access_secret,
			    remote_root,
			    remote_file,
			    byterange,
			    rev,
			    metadata,
			    error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

static void gdrop_object_get_async_threadfunc(GSimpleAsyncResult *res,
					      GObject *object,
					      GCancellable *cancellable)
{
  GdropObject *dropObject = GDROP_OBJECT(object);
  GPtrArray *args = g_simple_async_result_get_op_res_gpointer(res);
  gchar *remote_root = g_ptr_array_index(args, 0);
  gchar *remote_file = g_ptr_array_index(args, 1);
  gchar *byterange = g_ptr_array_index(args, 2);
  gchar *rev = g_ptr_array_index(args, 3);
  gchar **metadata = g_ptr_array_index(args, 4);
  GByteArray *byteArray = NULL;
  GError *error = NULL;

  byteArray = gdrop_object_get(dropObject,
			       remote_root,
			       remote_file,
			       byterange,
			       rev,
			       metadata,
			       &error);
  if(error) g_simple_async_result_take_error(res, error);
  g_simple_async_result_set_op_res_gpointer(res, byteArray, NULL);

  g_free(remote_root);
  g_free(remote_file);
  g_free(byterange);
  g_free(rev);
  g_ptr_array_free(args, FALSE);
}

void gdrop_object_get_async(GdropObject *dropObject,
			    gchar *remote_root,
			    gchar *remote_file,
			    gchar *byterange,
			    gchar *rev,
			    gchar **metadata,
			    GCancellable *cancellable,
			    GAsyncReadyCallback callback,
			    gpointer user_data)
{
  GSimpleAsyncResult *simpleAsyncResult = NULL;
  GPtrArray *args = NULL;

  simpleAsyncResult = g_simple_async_result_new(G_OBJECT(dropObject),
						callback,
						user_data,
						gdrop_object_get_async);
  g_simple_async_result_set_check_cancellable(simpleAsyncResult,
					      cancellable);

  args = g_ptr_array_new();
  g_ptr_array_add(args, g_strdup(remote_root));
  g_ptr_array_add(args, g_strdup(remote_file));
  g_ptr_array_add(args, g_strdup(byterange));
  g_ptr_array_add(args, g_strdup(rev));
  g_ptr_array_add(args, metadata);
  g_simple_async_result_set_op_res_gpointer(simpleAsyncResult,
					    args,
					    NULL);
  g_simple_async_result_run_in_thread(simpleAsyncResult,
				      gdrop_object_get_async_threadfunc,
				      G_PRIORITY_DEFAULT,
				      cancellable);

  g_object_unref(simpleAsyncResult);
}

GByteArray* gdrop_object_get_finish(GdropObject *dropObject,
				    GAsyncResult *result,
				    GError **error)
{
  GSimpleAsyncResult *asyncResult = G_SIMPLE_ASYNC_RESULT(result);

  if(g_simple_async_result_propagate_error(asyncResult, error))
    return NULL;
  else
    return g_simple_async_result_get_op_res_gpointer(asyncResult);
}

gchar* gdrop_object_metadata(GdropObject *dropObject,
			     gchar *remote_root,
			     gchar *remote_file,
			     gchar *file_limit,
			     gchar *hash,
			     gchar *list,
			     gchar *include_deleted,
			     gchar *rev,
			     gchar *locale,
			     GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_metadata(consumer_key,
				 consumer_secret,
				 access_key,
				 access_secret,
				 remote_root,
				 remote_file,
				 file_limit,
				 hash,
				 list,
				 include_deleted,
				 rev,
				 locale,
				 error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

static void gdrop_object_metadata_async_threadfunc(GSimpleAsyncResult *res,
						   GObject *object,
						   GCancellable *cancellable)
{
  GdropObject *dropObject = GDROP_OBJECT(object);
  GPtrArray *args = g_simple_async_result_get_op_res_gpointer(res);
  gchar *remote_root = g_ptr_array_index(args, 0);
  gchar *remote_file = g_ptr_array_index(args, 1);
  gchar *file_limit = g_ptr_array_index(args, 2);
  gchar *hash = g_ptr_array_index(args, 3);
  gchar *list = g_ptr_array_index(args, 4);
  gchar *include_deleted = g_ptr_array_index(args, 5);
  gchar *rev = g_ptr_array_index(args, 6);
  gchar *locale = g_ptr_array_index(args, 7);
  gchar *result = NULL;
  GError *error = NULL;

  result = gdrop_object_metadata(dropObject,
				 remote_root,
				 remote_file,
				 file_limit,
				 hash,
				 list,
				 include_deleted,
				 rev,
				 locale,
				 &error);
  if(error) g_simple_async_result_take_error(res, error);
  g_simple_async_result_set_op_res_gpointer(res, result, NULL);
  g_ptr_array_free(args, TRUE);
}

void gdrop_object_metadata_async(GdropObject *dropObject,
				 gchar *remote_root,
				 gchar *remote_file,
				 gchar *file_limit,
				 gchar *hash,
				 gchar *list,
				 gchar *include_deleted,
				 gchar *rev,
				 gchar *locale,
				 GCancellable *cancellable,
				 GAsyncReadyCallback callback,
				 gpointer user_data)
{
  GSimpleAsyncResult *simpleAsyncResult = NULL;
  GPtrArray *args = NULL;

  simpleAsyncResult = g_simple_async_result_new(G_OBJECT(dropObject),
						callback,
						user_data,
						gdrop_object_metadata_async);
  g_simple_async_result_set_check_cancellable(simpleAsyncResult,
					      cancellable);

  args = g_ptr_array_new();
  g_ptr_array_add(args, g_strdup(remote_root));
  g_ptr_array_add(args, g_strdup(remote_file));
  g_ptr_array_add(args, g_strdup(file_limit));
  g_ptr_array_add(args, g_strdup(hash));
  g_ptr_array_add(args, g_strdup(list));
  g_ptr_array_add(args, g_strdup(include_deleted));
  g_ptr_array_add(args, g_strdup(rev));
  g_ptr_array_add(args, g_strdup(locale));
  g_simple_async_result_set_op_res_gpointer(simpleAsyncResult,
					    args,
					    NULL);
  g_simple_async_result_run_in_thread(simpleAsyncResult,
				      gdrop_object_metadata_async_threadfunc,
				      G_PRIORITY_DEFAULT,
				      cancellable);

  g_object_unref(simpleAsyncResult);
}

gchar* gdrop_object_metadata_finish(GdropObject *dropObject,
				    GAsyncResult *result,
				    GError **error)
{
  GSimpleAsyncResult *asyncResult = G_SIMPLE_ASYNC_RESULT(result);

  if(g_simple_async_result_propagate_error(asyncResult, error))
    return NULL;
  else
    return g_simple_async_result_get_op_res_gpointer(asyncResult);
}

gchar* gdrop_object_delta(GdropObject *dropObject,
			  gchar *cursor,
			  gchar *locale,
			  GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_delta(consumer_key,
			      consumer_secret,
			      access_key,
			      access_secret,
			      cursor,
			      locale,
			      error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

static void gdrop_object_delta_async_threadfunc(GSimpleAsyncResult *res,
						GObject *object,
						GCancellable *cancellable)
{
  GdropObject *dropObject = GDROP_OBJECT(object);
  GPtrArray *args = g_simple_async_result_get_op_res_gpointer(res);
  gchar *cursor = g_ptr_array_index(args, 0);
  gchar *locale = g_ptr_array_index(args, 1);
  gchar *result = NULL;
  GError *error = NULL;

  result = gdrop_object_delta(dropObject,
			      cursor,
			      locale,
			      &error);
  if(error) g_simple_async_result_take_error(res, error);
  g_simple_async_result_set_op_res_gpointer(res, result, NULL);
  g_ptr_array_free(args, TRUE);
}

void gdrop_object_delta_async(GdropObject *dropObject,
			      gchar *cursor,
			      gchar *locale,
			      GCancellable *cancellable,
			      GAsyncReadyCallback callback,
			      gpointer user_data)
{
  GSimpleAsyncResult *simpleAsyncResult = NULL;
  GPtrArray *args = NULL;

  simpleAsyncResult = g_simple_async_result_new(G_OBJECT(dropObject),
						callback,
						user_data,
						gdrop_object_delta_async);
  g_simple_async_result_set_check_cancellable(simpleAsyncResult,
					      cancellable);

  args = g_ptr_array_new();
  g_ptr_array_add(args, g_strdup(cursor));
  g_ptr_array_add(args, g_strdup(locale));
  g_simple_async_result_set_op_res_gpointer(simpleAsyncResult,
					    args,
					    NULL);
  g_simple_async_result_run_in_thread(simpleAsyncResult,
				      gdrop_object_delta_async_threadfunc,
				      G_PRIORITY_DEFAULT,
				      cancellable);

  g_object_unref(simpleAsyncResult);
}

gchar* gdrop_object_delta_finish(GdropObject *dropObject,
				 GAsyncResult *result,
				 GError **error)
{
  GSimpleAsyncResult *asyncResult = G_SIMPLE_ASYNC_RESULT(result);

  if(g_simple_async_result_propagate_error(asyncResult, error))
    return NULL;
  else
    return g_simple_async_result_get_op_res_gpointer(asyncResult);
}

gchar* gdrop_object_revisions(GdropObject *dropObject,
			      gchar *remote_root,
			      gchar *remote_file,
			      gchar *rev_limit,
			      gchar *locale,
			      GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_revisions(consumer_key,
				  consumer_secret,
				  access_key,
				  access_secret,
				  remote_root,
				  remote_file,
				  rev_limit,
				  locale,
				  error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

static void gdrop_object_revisions_async_threadfunc(GSimpleAsyncResult *res,
						    GObject *object,
						    GCancellable *cancellable)
{
  GdropObject *dropObject = GDROP_OBJECT(object);
  GPtrArray *args = g_simple_async_result_get_op_res_gpointer(res);
  gchar *remote_root = g_ptr_array_index(args, 0);
  gchar *remote_file = g_ptr_array_index(args, 1);
  gchar *rev_limit = g_ptr_array_index(args, 2);
  gchar *locale = g_ptr_array_index(args, 3);
  gchar *result = NULL;
  GError *error = NULL;

  result = gdrop_object_revisions(dropObject,
				  remote_root,
				  remote_file,
				  rev_limit,
				  locale,
				  &error);
  if(error) g_simple_async_result_take_error(res, error);
  g_simple_async_result_set_op_res_gpointer(res, result, NULL);
  g_ptr_array_free(args, TRUE);
}

void gdrop_object_revisions_async(GdropObject *dropObject,
				  gchar *remote_root,
				  gchar *remote_file,
				  gchar *rev_limit,
				  gchar *locale,
				  GCancellable *cancellable,
				  GAsyncReadyCallback callback,
				  gpointer user_data)
{
  GSimpleAsyncResult *simpleAsyncResult = NULL;
  GPtrArray *args = NULL;

  simpleAsyncResult = g_simple_async_result_new(G_OBJECT(dropObject),
						callback,
						user_data,
						gdrop_object_revisions_async);
  g_simple_async_result_set_check_cancellable(simpleAsyncResult,
					      cancellable);

  args = g_ptr_array_new();
  g_ptr_array_add(args, g_strdup(remote_root));
  g_ptr_array_add(args, g_strdup(remote_file));
  g_ptr_array_add(args, g_strdup(rev_limit));
  g_ptr_array_add(args, g_strdup(locale));
  g_simple_async_result_set_op_res_gpointer(simpleAsyncResult,
					    args,
					    NULL);
  g_simple_async_result_run_in_thread(simpleAsyncResult,
				      gdrop_object_revisions_async_threadfunc,
				      G_PRIORITY_DEFAULT,
				      cancellable);

  g_object_unref(simpleAsyncResult);
}

gchar* gdrop_object_revisions_finish(GdropObject *dropObject,
				     GAsyncResult *result,
				     GError **error)
{
  GSimpleAsyncResult *asyncResult = G_SIMPLE_ASYNC_RESULT(result);

  if(g_simple_async_result_propagate_error(asyncResult, error))
    return NULL;
  else
    return g_simple_async_result_get_op_res_gpointer(asyncResult);
}

gchar* gdrop_object_restore(GdropObject *dropObject,
			    gchar *remote_root,
			    gchar *remote_file,
			    gchar *rev,
			    gchar *locale,
			    GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_restore(consumer_key,
				consumer_secret,
				access_key,
				access_secret,
				remote_root,
				remote_file,
				rev,
				locale,
				error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

gchar* gdrop_object_search(GdropObject *dropObject,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *query,
			   gchar *file_limit,
			   gchar *include_deleted,
			   gchar *locale,
			   GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_search(consumer_key,
			       consumer_secret,
			       access_key,
			       access_secret,
			       remote_root,
			       remote_file,
			       query,
			       file_limit,
			       include_deleted,
			       locale,
			       error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

static void gdrop_object_search_async_threadfunc(GSimpleAsyncResult *res,
						 GObject *object,
						 GCancellable *cancellable)
{
  GdropObject *dropObject = GDROP_OBJECT(object);
  GPtrArray *args = g_simple_async_result_get_op_res_gpointer(res);
  gchar *remote_root = g_ptr_array_index(args, 0);
  gchar *remote_file = g_ptr_array_index(args, 1);
  gchar *query = g_ptr_array_index(args, 2);
  gchar *file_limit = g_ptr_array_index(args, 3);
  gchar *include_deleted = g_ptr_array_index(args, 4);
  gchar *locale = g_ptr_array_index(args, 5);
  gchar *result = NULL;
  GError *error = NULL;

  result = gdrop_object_search(dropObject,
			       remote_root,
			       remote_file,
			       query,
			       file_limit,
			       include_deleted,
			       locale,
			       &error);
  if(error) g_simple_async_result_take_error(res, error);
  g_simple_async_result_set_op_res_gpointer(res, result, NULL);
  g_ptr_array_free(args, TRUE);
}

void gdrop_object_search_async(GdropObject *dropObject,
			       gchar *remote_root,
			       gchar *remote_file,
			       gchar *query,
			       gchar *file_limit,
			       gchar *include_deleted,
			       gchar *locale,
			       GCancellable *cancellable,
			       GAsyncReadyCallback callback,
			       gpointer user_data)
{
  GSimpleAsyncResult *simpleAsyncResult = NULL;
  GPtrArray *args = NULL;

  simpleAsyncResult = g_simple_async_result_new(G_OBJECT(dropObject),
						callback,
						user_data,
						gdrop_object_search_async);
  g_simple_async_result_set_check_cancellable(simpleAsyncResult,
					      cancellable);

  args = g_ptr_array_new();
  g_ptr_array_add(args, g_strdup(remote_root));
  g_ptr_array_add(args, g_strdup(remote_file));
  g_ptr_array_add(args, g_strdup(query));
  g_ptr_array_add(args, g_strdup(file_limit));
  g_ptr_array_add(args, g_strdup(include_deleted));
  g_ptr_array_add(args, g_strdup(locale));
  g_simple_async_result_set_op_res_gpointer(simpleAsyncResult,
					    args,
					    NULL);
  g_simple_async_result_run_in_thread(simpleAsyncResult,
				      gdrop_object_search_async_threadfunc,
				      G_PRIORITY_DEFAULT,
				      cancellable);

  g_object_unref(simpleAsyncResult);
}

gchar* gdrop_object_search_finish(GdropObject *dropObject,
				  GAsyncResult *result,
				  GError **error)
{
  GSimpleAsyncResult *asyncResult = G_SIMPLE_ASYNC_RESULT(result);

  if(g_simple_async_result_propagate_error(asyncResult, error))
    return NULL;
  else
    return g_simple_async_result_get_op_res_gpointer(asyncResult);
}

gchar* gdrop_object_shares(GdropObject *dropObject,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *locale,
			   gchar *short_url,
			   GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_shares(consumer_key,
			       consumer_secret,
			       access_key,
			       access_secret,
			       remote_root,
			       remote_file,
			       locale,
			       short_url,
			       error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

gchar* gdrop_object_media(GdropObject *dropObject,
			  gchar *remote_root,
			  gchar *remote_file,
			  gchar *locale,
			  GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_media(consumer_key,
			      consumer_secret,
			      access_key,
			      access_secret,
			      remote_root,
			      remote_file,
			      locale,
			      error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

gchar* gdrop_object_copy_ref(GdropObject *dropObject,
			     gchar *remote_root,
			     gchar *remote_file,
			     GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_copy_ref(consumer_key,
				 consumer_secret,
				 access_key,
				 access_secret,
				 remote_root,
				 remote_file,
				 error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

GByteArray* gdrop_object_thumbnails(GdropObject *dropObject,
				    gchar *remote_root,
				    gchar *remote_file,
				    gchar *format,
				    gchar *size,
				    gchar **metadata,
				    GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  GByteArray *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_thumbnails(consumer_key,
				   consumer_secret,
				   access_key,
				   access_secret,
				   remote_root,
				   remote_file,
				   format,
				   size,
				   metadata,
				   error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

static void gdrop_object_thumbnails_async_threadfunc(GSimpleAsyncResult *res,
						     GObject *object,
						     GCancellable *cancellable)
{
  GdropObject *dropObject = GDROP_OBJECT(object);
  GPtrArray *args = g_simple_async_result_get_op_res_gpointer(res);
  gchar *remote_root = g_ptr_array_index(args, 0);
  gchar *remote_file = g_ptr_array_index(args, 1);
  gchar *format = g_ptr_array_index(args, 2);
  gchar *size = g_ptr_array_index(args, 3);
  gchar **metadata = g_ptr_array_index(args, 4);
  GByteArray *byteArray = NULL;
  GError *error = NULL;

  byteArray = gdrop_object_thumbnails(dropObject,
				      remote_root,
				      remote_file,
				      format,
				      size,
				      metadata,
				      &error);
  if(error) g_simple_async_result_take_error(res, error);
  g_simple_async_result_set_op_res_gpointer(res, byteArray, NULL);

  g_free(remote_root);
  g_free(remote_file);
  g_free(format);
  g_free(size);
  g_ptr_array_free(args, FALSE);
}

void gdrop_object_thumbnails_async(GdropObject *dropObject,
				   gchar *remote_root,
				   gchar *remote_file,
				   gchar *format,
				   gchar *size,
				   gchar **metadata,
				   GCancellable *cancellable,
				   GAsyncReadyCallback callback,
				   gpointer user_data)
{
  GSimpleAsyncResult *simpleAsyncResult = NULL;
  GPtrArray *args = NULL;

  simpleAsyncResult = g_simple_async_result_new(G_OBJECT(dropObject),
						callback,
						user_data,
						gdrop_object_thumbnails_async);
  g_simple_async_result_set_check_cancellable(simpleAsyncResult,
					      cancellable);

  args = g_ptr_array_new();
  g_ptr_array_add(args, g_strdup(remote_root));
  g_ptr_array_add(args, g_strdup(remote_file));
  g_ptr_array_add(args, g_strdup(format));
  g_ptr_array_add(args, g_strdup(size));
  g_ptr_array_add(args, metadata);
  g_simple_async_result_set_op_res_gpointer(simpleAsyncResult,
					    args,
					    NULL);
  g_simple_async_result_run_in_thread(simpleAsyncResult,
				      gdrop_object_thumbnails_async_threadfunc,
				      G_PRIORITY_DEFAULT,
				      cancellable);

  g_object_unref(simpleAsyncResult);
}

GByteArray* gdrop_object_thumbnails_finish(GdropObject *dropObject,
					   GAsyncResult *result,
					   GError **error)
{
  GSimpleAsyncResult *asyncResult = G_SIMPLE_ASYNC_RESULT(result);

  if(g_simple_async_result_propagate_error(asyncResult, error))
    return NULL;
  else
    return g_simple_async_result_get_op_res_gpointer(asyncResult);
}

gchar* gdrop_object_chunked_upload(GdropObject *dropObject,
				   gpointer chunk,
				   gsize chunklength,
				   gchar *content_type,
				   gchar *upload_id,
				   gchar *offset,
				   GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_chunked_upload(consumer_key,
				       consumer_secret,
				       access_key,
				       access_secret,
				       chunk,
				       chunklength,
				       content_type,
				       upload_id,
				       offset,
				       error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

static void gdrop_object_chunked_upload_async_threadfunc(GSimpleAsyncResult *res,
							 GObject *object,
							 GCancellable *cancellable)
{
  GdropObject *dropObject = GDROP_OBJECT(object);
  GPtrArray *args = g_simple_async_result_get_op_res_gpointer(res);
  gpointer *chunk = g_ptr_array_index(args, 0);
  gsize chunklength = (gsize) g_ptr_array_index(args, 1);
  gchar *content_type = g_ptr_array_index(args, 2);
  gchar *upload_id = g_ptr_array_index(args, 3);
  gchar *offset = g_ptr_array_index(args, 4);
  gchar *result = NULL;
  GError *error = NULL;

  result = gdrop_object_chunked_upload(dropObject,
				       chunk,
				       chunklength,
				       content_type,
				       upload_id,
				       offset,
				       &error);
  if(error) g_simple_async_result_take_error(res, error);
  g_simple_async_result_set_op_res_gpointer(res, result, NULL);

  g_free(chunk);
  g_free(content_type);
  g_free(upload_id);
  g_free(offset);
  g_ptr_array_free(args, FALSE);
}

void gdrop_object_chunked_upload_async(GdropObject *dropObject,
				       gpointer chunk,
				       gsize chunklength,
				       gchar *content_type,
				       gchar *upload_id,
				       gchar *offset,
				       GCancellable *cancellable,
				       GAsyncReadyCallback callback,
				       gpointer user_data)
{
  GSimpleAsyncResult *simpleAsyncResult = NULL;
  GPtrArray *args = NULL;

  simpleAsyncResult = g_simple_async_result_new(G_OBJECT(dropObject),
						callback,
						user_data,
						gdrop_object_chunked_upload_async);
  g_simple_async_result_set_check_cancellable(simpleAsyncResult,
					      cancellable);

  args = g_ptr_array_new();
  g_ptr_array_add(args, g_memdup(chunk, chunklength));
  g_ptr_array_add(args, (gpointer) chunklength);
  g_ptr_array_add(args, g_strdup(content_type));
  g_ptr_array_add(args, g_strdup(upload_id));
  g_ptr_array_add(args, g_strdup(offset));
  g_simple_async_result_set_op_res_gpointer(simpleAsyncResult,
					    args,
					    NULL);
  g_simple_async_result_run_in_thread(simpleAsyncResult,
				      gdrop_object_chunked_upload_async_threadfunc,
				      G_PRIORITY_DEFAULT,
				      cancellable);

  g_object_unref(simpleAsyncResult);
}

gchar* gdrop_object_chunked_upload_finish(GdropObject *dropObject,
					  GAsyncResult *result,
					  GError **error)
{
  GSimpleAsyncResult *asyncResult = G_SIMPLE_ASYNC_RESULT(result);

  if(g_simple_async_result_propagate_error(asyncResult, error))
    return NULL;
  else
    return g_simple_async_result_get_op_res_gpointer(asyncResult);
}

gchar* gdrop_object_commit_chunked_upload(GdropObject *dropObject,
					  gchar *remote_root,
					  gchar *remote_file,
					  gchar *locale,
					  gchar *overwrite,
					  gchar *parent_rev,
					  gchar *upload_id,
					  GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_commit_chunked_upload(consumer_key,
					      consumer_secret,
					      access_key,
					      access_secret,
					      remote_root,
					      remote_file,
					      locale,
					      overwrite,
					      parent_rev,
					      upload_id,
					      error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

gchar* gdrop_object_copy(GdropObject *dropObject,
			 gchar *remote_root,
			 gchar *from_path,
			 gchar *to_path,
			 gchar *locale,
			 gchar *from_copy_ref,
			 GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_copy(consumer_key,
			     consumer_secret,
			     access_key,
			     access_secret,
			     remote_root,
			     from_path,
			     to_path,
			     locale,
			     from_copy_ref,
			     error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

gchar* gdrop_object_create_folder(GdropObject *dropObject,
				  gchar *remote_root,
				  gchar *remote_file,
				  gchar *locale,
				  GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_create_folder(consumer_key,
				      consumer_secret,
				      access_key,
				      access_secret,
				      remote_root,
				      remote_file,
				      locale,
				      error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

gchar* gdrop_object_delete(GdropObject *dropObject,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *locale,
			   GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_delete(consumer_key,
			       consumer_secret,
			       access_key,
			       access_secret,
			       remote_root,
			       remote_file,
			       locale,
			       error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}

gchar* gdrop_object_move(GdropObject *dropObject,
			 gchar *remote_root,
			 gchar *from_path,
			 gchar *to_path,
			 gchar *locale,
			 GError **error)
{
  gchar *consumer_key = NULL;
  gchar *consumer_secret = NULL;
  gchar *access_key = NULL;
  gchar *access_secret = NULL;
  gchar *result = NULL;
  
  g_object_get(G_OBJECT(dropObject),
               "consumer_key", &consumer_key,
               "consumer_secret", &consumer_secret,
               "access_key", &access_key,
               "access_secret", &access_secret,
               NULL);

  result = drop_dropbox_move(consumer_key,
			     consumer_secret,
			     access_key,
			     access_secret,
			     remote_root,
			     from_path,
			     to_path,
			     locale,
			     error);

  g_free(consumer_key);
  g_free(consumer_secret);
  g_free(access_key);
  g_free(access_secret);

  return result;
}
