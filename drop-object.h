#ifndef __GDROP_OBJECT_H__
#define __GDROP_OBJECT_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GDROP_TYPE_OBJECT (gdrop_object_get_type())
#define GDROP_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), GDROP_TYPE_OBJECT, GdropObject))
#define GDROP_IS_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GDROP_TYPE_OBJECT))
#define GDROP_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), GDROP_TYPE_OBJECT, GdropObjectClass))
#define GDROP_IS_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GDROP_TYPE_OBJECT))
#define GDROP_OBJECT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), GDROP_TYPE_OBJECT, GdropObjectClass))

typedef struct _GdropObjectClass GdropObjectClass;
typedef struct _GdropObject GdropObject;

struct _GdropObjectClass
{
  GObjectClass parentClass;
};

struct _GdropObject
{
  GObject parent;

  /*<private>*/
  gchar *appname;
  gchar *consumer_key;
  gchar *consumer_secret;
  gchar *request_key;
  gchar *request_secret;
  gchar *access_key;
  gchar *access_secret;
};

enum
  {
    PROP_0,
    APPNAME,
    CONSUMER_KEY,
    CONSUMER_SECRET,
    REQUEST_KEY,
    REQUEST_SECRET,
    ACCESS_KEY,
    ACCESS_SECRET
  };

GType gdrop_object_get_type(void) G_GNUC_CONST;

/**
 * gdrop_object_new:
 */
GdropObject* gdrop_object_new(void);

/**
 * gdrop_object_initkeys:
 * @dropObject: a GdropObject
 */
gboolean gdrop_object_initkeys(GdropObject *dropObject);

/**
 * gdrop_object_gen_authurl:
 * @dropObject: a GdropObject
 * @consumer_key: dropbox app consumer key
 * @consumer_secret: dropbox app consumer secret
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_gen_authurl(GdropObject *dropObject,
				gchar *consumer_key,
				gchar *consumer_secret,
				GError **error);

/**
 * gdrop_object_auth:
 * @dropObject: a GdropObject
 * @appname: application name
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gboolean gdrop_object_auth(GdropObject *dropObject,
			   gchar *appname,
			   GError **error);

/**
 * gdrop_object_accountinfo:
 * @dropObject: a GdropObject
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_accountinfo(GdropObject *dropObject,
				gchar *locale,
				GError **error);

/**
 * gdrop_object_put:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root, either 'dropbox' or 'sandbox'
 * @remote_file: remote filepath
 * @filename: locale filename
 * @content_type: (allow-none): content type
 * @locale: (allow-none): output locale
 * @overwrite: (allow-none): either 'true' or 'false', will overwrite
 * @parent_rev: (allow-none): revision for the remote file
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_put(GdropObject *dropObject,
			gchar *remote_root,
			gchar *remote_file,
			gchar *filename,
			gchar *content_type,
			gchar *locale,
			gchar *overwrite,
			gchar *parent_rev,
			GError **error);

/**
 * gdrop_object_put_async:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root, either 'dropbox' or 'sandbox'
 * @remote_file: remote filepath
 * @filename: locale filename
 * @content_type: (allow-none): content type
 * @locale: (allow-none): output locale
 * @overwrite: (allow-none): either 'true' or 'false', will overwrite
 * @parent_rev: (allow-none): revision for the remote file
 * @cancellable: (allow-none): a GCancellable
 * @callback: (closure user_data) (scope async): a GAsyncReadyCallback function
 * @user_data: (closure) (allow-none): user_data for callback
 */
void gdrop_object_put_async(GdropObject *dropObject,
			    gchar *remote_root,
			    gchar *remote_file,
			    gchar *filename,
			    gchar *content_type,
			    gchar *locale,
			    gchar *overwrite,
			    gchar *parent_rev,
			    GCancellable *cancellable,
			    GAsyncReadyCallback callback,
			    gpointer user_data);

/**
 * gdrop_object_put_finish:
 * @dropObject: a GdropObject
 * @result: a GAsyncResult object
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_put_finish(GdropObject *dropObject,
			       GAsyncResult *result,
			       GError **error);

/**
 * gdrop_object_get:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @byterange: (allow-none): byte range to get eg: 0-100
 * @rev: (allow-none): revision to get
 * @metadata: (out callee-allocates) (allow-none): metadata for the file
 * @error: (out callee-allocates) (allow-none): a GError from soup
 *
 * Returns: (transfer full): a GByteArray contains data
 */
GByteArray* gdrop_object_get(GdropObject *dropObject,
			     gchar *remote_root,
			     gchar *remote_file,
			     gchar *byterange,
			     gchar *rev,
			     gchar **metadata,
			     GError **error);

/**
 * gdrop_object_get_async:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @byterange: (allow-none): byte range to get eg: 0-100
 * @rev: (allow-none): revision to get
 * @metadata: (out callee-allocates) (allow-none): metadata for the file
 * @cancellable: (allow-none): a GCancellable
 * @callback: (closure user_data) (scope async): a GAsyncReadyCallback function
 * @user_data: (closure) (allow-none): user_data for callback
 */
void gdrop_object_get_async(GdropObject *dropObject,
			    gchar *remote_root,
			    gchar *remote_file,
			    gchar *byterange,
			    gchar *rev,
			    gchar **metadata,
			    GCancellable *cancellable,
			    GAsyncReadyCallback callback,
			    gpointer user_data);

/**
 * gdrop_object_get_finish:
 * @dropObject: a GdropObject
 * @result: a GAsyncResult object
 * @error: (out callee-allocates) (allow-none): a GError from soup
 *
 * Returns: (transfer full): a GByteArray contains data
 */
GByteArray* gdrop_object_get_finish(GdropObject *dropObject,
				    GAsyncResult *result,
				    GError **error);

/**
 * gdrop_object_metadata:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @file_limit: (allow-none): maximum number of files listing in a folder
 * @hash: (allow-none): hash tag for the next metadata call
 * @list: (allow-none): true or false, will include contents metadata
 * @include_deleted: (allow-none): true or false, will include deleted file metadata
 * @rev: (allow-none): revision to get metadata
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_metadata(GdropObject *dropObject,
			     gchar *remote_root,
			     gchar *remote_file,
			     gchar *file_limit,
			     gchar *hash,
			     gchar *list,
			     gchar *include_deleted,
			     gchar *rev,
			     gchar *locale,
			     GError **error);

/**
 * gdrop_object_metadata_async:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @file_limit: (allow-none): maximum number of files listing in a folder
 * @hash: (allow-none): hash tag for the next metadata call
 * @list: (allow-none): true or false, will include contents metadata
 * @include_deleted: (allow-none): true or false, will include deleted file metadata
 * @rev: (allow-none): revision to get metadata
 * @locale: (allow-none): output locale
 * @cancellable: (allow-none): a GCancellable
 * @callback: (closure user_data) (scope async): a GAsyncReadyCallback function
 * @user_data: (closure) (allow-none): user_data for callback
 */
void gdrop_object_metadata_async(GdropObject *dropObject,
				 gchar *remote_root,
				 gchar *remote_file,
				 gchar *file_limit,
				 gchar *hash,
				 gchar *list,
				 gchar *include_deleted,
				 gchar *rev,
				 gchar *locale,
				 GCancellable *cancellable,
				 GAsyncReadyCallback callback,
				 gpointer user_data);

/**
 * gdrop_object_metadata_finish:
 * @dropObject: a GdropObject
 * @result: a GAsyncResult object
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_metadata_finish(GdropObject *dropObject,
				    GAsyncResult *result,
				    GError **error);

/**
 * gdrop_object_delta:
 * @dropObject: a GdropObject
 * @cursor: (allow-none): cursor returned in last call
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_delta(GdropObject *dropObject,
			  gchar *cursor,
			  gchar *locale,
			  GError **error);

/**
 * gdrop_object_delta_async:
 * @dropObject: a GdropObject
 * @cursor: (allow-none): cursor returned in last call
 * @locale: (allow-none): output locale
 * @cancellable: (allow-none): a GCancellable
 * @callback: (closure user_data) (scope async): a GAsyncReadyCallback function
 * @user_data: (closure) (allow-none): user_data for callback
 */
void gdrop_object_delta_async(GdropObject *dropObject,
			      gchar *cursor,
			      gchar *locale,
			      GCancellable *cancellable,
			      GAsyncReadyCallback callback,
			      gpointer user_data);

/**
 * gdrop_object_delta_finish:
 * @dropObject: a GdropObject
 * @result: a GAsyncResult object
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_delta_finish(GdropObject *dropObject,
				 GAsyncResult *result,
				 GError **error);

/**
 * gdrop_object_revisions:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root, either 'dropbox' or 'sandbox'
 * @remote_file: remote filepath
 * @rev_limit: (allow-none): revision limit
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_revisions(GdropObject *dropObject,
			      gchar *remote_root,
			      gchar *remote_file,
			      gchar *rev_limit,
			      gchar *locale,
			      GError **error);

/**
 * gdrop_object_revisions_async:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root, either 'dropbox' or 'sandbox'
 * @remote_file: remote filepath
 * @rev_limit: (allow-none): revision limit
 * @locale: (allow-none): output locale
 * @cancellable: (allow-none): a GCancellable
 * @callback: (closure user_data) (scope async): a GAsyncReadyCallback function
 * @user_data: (closure) (allow-none): user_data for callback
 */
void gdrop_object_revisions_async(GdropObject *dropObject,
				  gchar *remote_root,
				  gchar *remote_file,
				  gchar *rev_limit,
				  gchar *locale,
				  GCancellable *cancellable,
				  GAsyncReadyCallback callback,
				  gpointer user_data);

/**
 * gdrop_object_revisions_finish:
 * @dropObject: a GdropObject
 * @result: a GAsyncResult object
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_revisions_finish(GdropObject *dropObject,
				     GAsyncResult *result,
				     GError **error);

/**
 * gdrop_object_restore:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @rev: revision to restore
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_restore(GdropObject *dropObject,
			    gchar *remote_root,
			    gchar *remote_file,
			    gchar *rev,
			    gchar *locale,
			    GError **error);

/**
 * gdrop_object_search:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @query: filename to search
 * @file_limit: (allow-none): number of files to show
 * @include_deleted: (allow-none): 'true' or 'false', to show deleted files
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_search(GdropObject *dropObject,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *query,
			   gchar *file_limit,
			   gchar *include_deleted,
			   gchar *locale,
			   GError **error);

/**
 * gdrop_object_search_async:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @query: filename to search
 * @file_limit: (allow-none): number of files to show
 * @include_deleted: (allow-none): 'true' or 'false', to show deleted files
 * @locale: (allow-none): output locale
 * @cancellable: (allow-none): a GCancellable
 * @callback: (closure user_data) (scope async): a GAsyncReadyCallback function
 * @user_data: (closure) (allow-none): user_data for callback
 */
void gdrop_object_search_async(GdropObject *dropObject,
			       gchar *remote_root,
			       gchar *remote_file,
			       gchar *query,
			       gchar *file_limit,
			       gchar *include_deleted,
			       gchar *locale,
			       GCancellable *cancellable,
			       GAsyncReadyCallback callback,
			       gpointer user_data);

/**
 * gdrop_object_metadata_finish:
 * @dropObject: a GdropObject
 * @result: a GAsyncResult object
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_search_finish(GdropObject *dropObject,
				  GAsyncResult *result,
				  GError **error);

/**
 * gdrop_object_shares:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @locale: (allow-none): output locale
 * @short_url: (allow-none): either 'true' or 'false', to get short-url
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_shares(GdropObject *dropObject,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *locale,
			   gchar *short_url,
			   GError **error);

/**
 * gdrop_object_media:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_media(GdropObject *dropObject,
			  gchar *remote_root,
			  gchar *remote_file,
			  gchar *locale,
			  GError **error);

/**
 * gdrop_object_copy_ref:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_copy_ref(GdropObject *dropObject,
			     gchar *remote_root,
			     gchar *remote_file,
			     GError **error);

/**
 * gdrop_object_thumbnails:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @format: (allow-none): either 'jpeg' or 'png'
 * @size: (allow-none): values 'xs', 's', 'm', 'l', 'xl'
 * @metadata: (out callee-allocates) (allow-none): metadata for that file
 * @error: (out callee-allocates) (allow-none): a GError from soup
 *
 * Returns: (transfer full): a GByteArray contains data
 */
GByteArray* gdrop_object_thumbnails(GdropObject *dropObject,
				    gchar *remote_root,
				    gchar *remote_file,
				    gchar *format,
				    gchar *size,
				    gchar **metadata,
				    GError **error);

/**
 * gdrop_object_thumbnails_async:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @format: (allow-none): either 'jpeg' or 'png'
 * @size: (allow-none): values 'xs', 's', 'm', 'l', 'xl'
 * @metadata: (out callee-allocates) (allow-none): metadata for that file
 * @cancellable: (allow-none): a GCancellable
 * @callback: (closure user_data) (scope async): a GAsyncReadyCallback function
 * @user_data: (closure) (allow-none): user_data for callback
 */
void gdrop_object_thumbnails_async(GdropObject *dropObject,
				   gchar *remote_root,
				   gchar *remote_file,
				   gchar *format,
				   gchar *size,
				   gchar **metadata,
				   GCancellable *cancellable,
				   GAsyncReadyCallback callback,
				   gpointer user_data);

/**
 * gdrop_object_thumbnails_finish:
 * @dropObject: a GdropObject
 * @result: a GAsyncResult object
 * @error: (out callee-allocates) (allow-none): a GError from soup
 *
 * Returns: (transfer full): a GByteArray contains data
 */
GByteArray* gdrop_object_thumbnails_finish(GdropObject *dropObject,
					   GAsyncResult *result,
					   GError **error);

/**
 * gdrop_object_chunked_upload:
 * @dropObject: a GdropObject
 * @chunk: data to upload
 * @chunklength: size of the data to upload
 * @content_type: (allow-none): type of the data
 * @upload_id: (allow-none): id returned in previous call
 * @offset: (allow-none): offset returned in previous call
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_chunked_upload(GdropObject *dropObject,
				   gpointer chunk,
				   gsize chunklength,
				   gchar *content_type,
				   gchar *upload_id,
				   gchar *offset,
				   GError **error);

/**
 * gdrop_object_chunked_upload_async:
 * @dropObject: a GdropObject
 * @chunk: data to upload
 * @chunklength: size of the data to upload
 * @content_type: (allow-none): type of the data
 * @upload_id: (allow-none): id returned in previous call
 * @offset: (allow-none): offset returned in previous call
 * @cancellable: (allow-none): a GCancellable
 * @callback: (closure user_data) (scope async): a GAsyncReadyCallback function
 * @user_data: (closure) (allow-none): user_data for callback
 */
void gdrop_object_chunked_upload_async(GdropObject *dropObject,
				       gpointer chunk,
				       gsize chunklength,
				       gchar *content_type,
				       gchar *upload_id,
				       gchar *offset,
				       GCancellable *cancellable,
				       GAsyncReadyCallback callback,
				       gpointer user_data);

/**
 * gdrop_object_chunked_upload_finish:
 * @dropObject: a GdropObject
 * @result: a GAsyncResult object
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_chunked_upload_finish(GdropObject *dropObject,
					  GAsyncResult *result,
					  GError **error);

/**
 * gdrop_object_commit_chunked_upload:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @locale: (allow-none): output locale
 * @overwrite: (allow-none): 'true' or 'false', to overwrite existing file
 * @parent_rev: (allow-none): revison to set for the new file
 * @upload_id: id returned in last chunked_upload call
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_commit_chunked_upload(GdropObject *dropObject,
					  gchar *remote_root,
					  gchar *remote_file,
					  gchar *locale,
					  gchar *overwrite,
					  gchar *parent_rev,
					  gchar *upload_id,
					  GError **error);

/**
 * gdrop_object_copy:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @from_path: remote source filepath
 * @to_path: remote dest filepath
 * @locale: (allow-none): output locale
 * @from_copy_ref: (allow-none): returned from copy_ref call
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_copy(GdropObject *dropObject,
			 gchar *remote_root,
			 gchar *from_path,
			 gchar *to_path,
			 gchar *locale,
			 gchar *from_copy_ref,
			 GError **error);

/**
 * gdrop_object_create_folder:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_create_folder(GdropObject *dropObject,
				  gchar *remote_root,
				  gchar *remote_file,
				  gchar *locale,
				  GError **error);

/**
 * gdrop_object_delete:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @remote_file: remote filepath
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_delete(GdropObject *dropObject,
			   gchar *remote_root,
			   gchar *remote_file,
			   gchar *locale,
			   GError **error);

/**
 * gdrop_object_move:
 * @dropObject: a GdropObject
 * @remote_root: dropbox root
 * @from_path: remote source filepath
 * @to_path: remote dest filepath
 * @locale: (allow-none): output locale
 * @error: (out callee-allocates) (allow-none): a GError from soup
 */
gchar* gdrop_object_move(GdropObject *dropObject,
			 gchar *remote_root,
			 gchar *from_path,
			 gchar *to_path,
			 gchar *locale,
			 GError **error);

G_END_DECLS

#endif /* __GDROP_OBJECT_H__ */
