#include <drop.h>

static drop_soup_set_error(GError **error, guint http_code)
{
  if(*error)
    {
      g_error_free(*error);
      *error = NULL;
    }
  g_set_error(error,
	      g_quark_from_string("libsoup"),
	      http_code,
	      "%s",
	      soup_status_get_phrase(http_code));
}

static gchar* drop_soup_get_oauthheader(gchar **url,
					gchar **params)
{
  gchar *baseurl = NULL;
  gchar **paramsv = NULL;
  GString *oauthparams = g_string_new(NULL);
  GString *leftparams = g_string_new(NULL);
  gchar *result = NULL;
  guint iter = 0;

  oauthparams = g_string_new("OAuth ");
  if(*params)
    {
      baseurl = g_strdup(*url);
      paramsv = g_strsplit(*params, "&", 0);
    }
  else
    {
      gchar **urlv = NULL;

      urlv = g_strsplit(*url, "?", 2);
      baseurl = g_strdup(urlv[0]);
      if(urlv[1] && strlen(urlv[1]))
	paramsv = g_strsplit(urlv[1], "&", 0);
      g_strfreev(urlv);
    }

  if(paramsv)
    {
      while(paramsv[iter])
	{
	  if(g_str_has_prefix(paramsv[iter], "oauth_"))
	    {
	      gchar **fieldv = g_strsplit(paramsv[iter], "=", 2);
	      g_string_append_printf(oauthparams,
				     "%s=\"%s\", ",
				     fieldv[0],
				     fieldv[1]);
	      g_strfreev(fieldv);
	    }
	  else
	    g_string_append_printf(leftparams, "&%s", paramsv[iter]);

	  iter++;
	}
      g_string_truncate(oauthparams, oauthparams->len - 2);
      if(leftparams->len)
	g_string_erase(leftparams, 0, 1);

      if(*params == NULL)
	{
	  g_free(*url);
	  if(leftparams->len)
	    *url = g_strdup_printf("%s?%s", baseurl, leftparams->str);
	  else
	    *url = g_strdup(baseurl);
	}
      else
	{
	  g_free(*url);
	  *url = g_strdup(baseurl);
	  if(leftparams->len)
	    {
	      g_free(*params);
	      *params = g_strdup(leftparams->str);
	    }
	  else
	    {
	      g_free(*params);
	      *params = NULL;
	    }
	}
      g_strfreev(paramsv);
    }

  g_free(baseurl);
  g_string_free(leftparams, TRUE);
  result = g_strdup(oauthparams->str);
  g_string_free(oauthparams, TRUE);
  return result;
}

static gchar* drop_soup_get_rangeheader(gchar **url,
					gchar **params)
{
  gchar *baseurl = NULL;
  gchar **paramsv = NULL;
  GString *rangeparams = g_string_new(NULL);
  GString *leftparams = g_string_new(NULL);
  gchar *result = NULL;
  guint iter = 0;

  rangeparams = g_string_new(NULL);
  if(*params)
    {
      baseurl = g_strdup(*url);
      paramsv = g_strsplit(*params, "&", 0);
    }
  else
    {
      gchar **urlv = NULL;

      urlv = g_strsplit(*url, "?", 2);
      baseurl = g_strdup(urlv[0]);
      if(urlv[1] && strlen(urlv[1]))
	paramsv = g_strsplit(urlv[1], "&", 0);
      g_strfreev(urlv);
    }

  if(paramsv)
    {
      while(paramsv[iter])
	{
	  if(g_str_has_prefix(paramsv[iter], "bytes="))
	    g_string_assign(rangeparams, paramsv[iter]);
	  else
	    g_string_append_printf(leftparams, "&%s", paramsv[iter]);

	  iter++;
	}
      if(leftparams->len)
	g_string_erase(leftparams, 0, 1);

      if(*params == NULL)
	{
	  g_free(*url);
	  if(leftparams->len)
	    *url = g_strdup_printf("%s?%s", baseurl, leftparams->str);
	  else
	    *url = g_strdup(baseurl);
	}
      else
	{
	  g_free(*url);
	  *url = g_strdup(baseurl);
	  if(leftparams->len)
	    {
	      g_free(*params);
	      *params = g_strdup(leftparams->str);
	    }
	  else
	    {
	      g_free(*params);
	      *params = NULL;
	    }
	}
      g_strfreev(paramsv);
    }

  g_free(baseurl);
  g_string_free(leftparams, TRUE);
  result = g_strdup(rangeparams->str);
  g_string_free(rangeparams, TRUE);
  return result;
}

void drop_soup_init(void)
{
  drop_soup_session = soup_session_sync_new();
  drop_soup_logger = soup_logger_new(SOUP_LOGGER_LOG_BODY, -1);
  soup_logger_attach(drop_soup_logger,
  		     drop_soup_session);
}

void drop_soup_free(void)
{
  soup_logger_detach(drop_soup_logger,
  		     drop_soup_session);
  g_object_unref(drop_soup_logger);
  g_object_unref(drop_soup_session);
}

gchar* drop_soup_sync(gchar *inputurl,
		      gchar *inputparams,
		      guint flags,
		      GError **error)
{
  GByteArray *buffer = drop_soup_sync_bytes(inputurl,
					    inputparams,
					    flags,
					    NULL,
					    NULL,
					    error);
  gchar *string = g_strndup(buffer->data, buffer->len);
  g_byte_array_free(buffer, TRUE);
  return string;
}

GByteArray* drop_soup_sync_bytes(gchar *inputurl,
				 gchar *inputparams,
				 guint flags,
				 gchar *responseheadername,
				 gchar **responseheader,
				 GError **error)
{
  gchar *url = g_strdup(inputurl);
  gchar *params = g_strdup(inputparams);
  GByteArray *buffer = g_byte_array_new();
  SoupMessage *msg = NULL;
  gchar *authparams = NULL;
  gchar *rangeparams = NULL;
  guint http_code = 0;

  if(flags & DROP_SOUP_OAUTH)
    authparams = drop_soup_get_oauthheader(&url, &params);

  if(flags & DROP_SOUP_RANGE)
    rangeparams = drop_soup_get_rangeheader(&url, &params);

  if(inputparams)
    {
      msg = soup_message_new("POST", url);
      if(params)
	soup_message_set_request(msg,
				 "application/x-www-form-urlencoded",
				 SOUP_MEMORY_COPY,
				 params,
				 strlen(params));
    }
  else
    msg = soup_message_new("GET", url);

  if(authparams)
    soup_message_headers_append(msg->request_headers,
				"Authorization", authparams);

  if(rangeparams)
    soup_message_headers_append(msg->request_headers,
				"Range", rangeparams);

  http_code = soup_session_send_message(drop_soup_session, msg);
  if(http_code != 200) drop_soup_set_error(error, http_code);
  g_byte_array_append(buffer,
		      msg->response_body->data,
		      msg->response_body->length);

  if(responseheadername && responseheader)
    *responseheader = g_strdup(soup_message_headers_get_one(msg->response_headers,
							    responseheadername));

  g_free(url);
  g_free(params);
  g_free(authparams);
  g_free(rangeparams);
  g_object_unref(msg);

  return buffer;
}

gchar* drop_soup_put(gchar *inputurl,
		     gchar *filename,
		     gpointer chunk,
		     gsize chunklength,
		     gchar *content_type,
		     guint flags,
		     GError **error)
{
  gchar *url = g_strdup(inputurl);
  gchar *params = NULL;
  gchar *result = NULL;
  gchar *filecontent = NULL;
  gsize filelength;
  gchar *filelengthstr = NULL;
  SoupMessage *msg = NULL;
  gchar *authparams = NULL;
  guint http_code = 0;
  GError *fileError = NULL;

  if(flags & DROP_SOUP_OAUTH)
    authparams = drop_soup_get_oauthheader(&url, &params);

  if(filename)
    {
      g_file_get_contents(filename, &filecontent, &filelength, &fileError);
      if(fileError) g_printerr("%s(%d)\n", fileError->message, fileError->code);
    }
  else
    {
      filecontent = g_memdup(chunk, chunklength);
      filelength = chunklength;
    }

  msg = soup_message_new("PUT", url);
  soup_message_set_request(msg,
			   content_type,
			   SOUP_MEMORY_COPY,
			   filecontent,
			   filelength);
  filelengthstr = g_strdup_printf("%ld", filelength);
  soup_message_headers_append(msg->request_headers,
			      "Content-Length", filelengthstr);

  if(authparams)
    soup_message_headers_append(msg->request_headers,
				"Authorization", authparams);

  http_code = soup_session_send_message(drop_soup_session, msg);
  if(http_code != 200) drop_soup_set_error(error, http_code);
  result = g_strndup(msg->response_body->data, msg->response_body->length);

  g_free(url);
  g_free(filecontent);
  g_free(filelengthstr);
  g_free(params);
  g_free(authparams);
  g_object_unref(msg);

  return result;
}
