#ifndef __DROP_SOUP_H__
#define __DROP_SOUP_H__

#include <libsoup/soup.h>

typedef enum {
  DROP_SOUP_OAUTH = 1,
  DROP_SOUP_RANGE = 2,
} DropSoupFlags;

void drop_soup_init(void);
void drop_soup_free(void);

gchar* drop_soup_sync(gchar *inputurl,
		      gchar *inputparams,
		      guint flags,
		      GError **error);
GByteArray* drop_soup_sync_bytes(gchar *inputurl,
				 gchar *inputparams,
				 guint flags,
				 gchar *responseheadername,
				 gchar **responseheader,
				 GError **error);
gchar* drop_soup_put(gchar *inputurl,
		     gchar *filename,
		     gpointer chunk,
		     gsize chunklength,
		     gchar *content_type,
		     guint flags,
		     GError **error);

SoupSession *drop_soup_session;
SoupLogger *drop_soup_logger;

#endif /* __DROP_SOUP_H__ */
