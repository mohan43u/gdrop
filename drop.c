#include <drop.h>

#define APPNAME "libdrop"
#define CONSUMER_KEY "5dtht4633w56g9f"
#define CONSUMER_SECRET "cho6ngtsjp3gbiw"
#define CHUNKLENGTHMAX 1048576

static void checkerror(GError *error)
{
  if(error)
    {
      g_printerr("%s:%d: %s\n",
		 g_quark_to_string(error->domain),
		 error->code,
		 error->message);
      if(error->code > 400 || error->code < 200)
	exit(EXIT_FAILURE);
    }
}

static void gdrop_object_get_async_callback(GObject *source_object,
					    GAsyncResult *res,
					    gpointer user_data)
{
  GError *error = NULL;
  gchar *filename = (gchar *) user_data;
  GByteArray *filecontent = gdrop_object_get_finish(GDROP_OBJECT(source_object),
						    res,
						    &error);
  checkerror(error);
  g_file_set_contents(filename,
  		      filecontent->data,
  		      filecontent->len,
   		      NULL);
}

static void gdrop_object_put_async_callback(GObject *source_object,
					    GAsyncResult *res,
					    gpointer user_data)
{
  GError *error = NULL;
  gchar *result = NULL;
  result = gdrop_object_put_finish(GDROP_OBJECT(source_object),
				   res,
				   &error);
  checkerror(error);
  g_print("%s\n", result);
}

static void gdrop_object_thumbnails_async_callback(GObject *source_object,
						   GAsyncResult *res,
						   gpointer user_data)
{
  GError *error = NULL;
  gchar *filename = (gchar *) user_data;
  GByteArray *filecontent = gdrop_object_thumbnails_finish(GDROP_OBJECT(source_object),
							   res,
							   &error);
  checkerror(error);
  g_file_set_contents(filename,
  		      filecontent->data,
  		      filecontent->len,
   		      NULL);
}

static void gdrop_object_metadata_async_callback(GObject *source_object,
						 GAsyncResult *res,
						 gpointer user_data)
{
  GError *error = NULL;
  gchar *result = NULL;
  result = gdrop_object_metadata_finish(GDROP_OBJECT(source_object),
					res,
					&error);
  checkerror(error);
  g_print("%s\n", result);
}

static void gdrop_object_search_async_callback(GObject *source_object,
					       GAsyncResult *res,
					       gpointer user_data)
{
  GError *error = NULL;
  gchar *result = NULL;
  result = gdrop_object_search_finish(GDROP_OBJECT(source_object),
				      res,
				      &error);
  checkerror(error);
  g_print("%s\n", result);
}

static void gdrop_object_delta_async_callback(GObject *source_object,
					      GAsyncResult *res,
					      gpointer user_data)
{
  GError *error = NULL;
  gchar *result = NULL;
  result = gdrop_object_search_finish(GDROP_OBJECT(source_object),
				      res,
				      &error);
  checkerror(error);
  g_print("%s\n", result);
}

static void gdrop_object_revisions_async_callback(GObject *source_object,
						  GAsyncResult *res,
						  gpointer user_data)
{
  GError *error = NULL;
  gchar *result = NULL;
  result = gdrop_object_search_finish(GDROP_OBJECT(source_object),
				      res,
				      &error);
  checkerror(error);
  g_print("%s\n", result);
}

int main(int argc, char *argv[])
{
  GdropObject *dropObject = NULL;
  GByteArray *filecontent = NULL;
  GError *error = NULL;

  g_type_init();

  dropObject = gdrop_object_new();
  if(!gdrop_object_initkeys(dropObject))
    {
      gchar *url = NULL;
      gchar *cmdline = NULL;

      url = gdrop_object_gen_authurl(dropObject,
				     CONSUMER_KEY,
				     CONSUMER_SECRET,
				     &error);
      checkerror(error);
      cmdline = g_strdup_printf("xdg-open '%s'", url);
      g_spawn_command_line_sync(cmdline, NULL, NULL, NULL, NULL);
      readline("Press any key to continue once you are done with authorization..");
      gdrop_object_auth(dropObject, "libgdrop", &error);
      checkerror(error);
    }
  else
    {
      g_print("%s\n%s\n%s\n%s\n%s\n%s\n%s\n",
	      dropObject->appname,
	      dropObject->consumer_key,
	      dropObject->consumer_secret,
	      dropObject->request_key,
	      dropObject->request_secret,
	      dropObject->access_key,
	      dropObject->access_secret);
    }

  /* 
   * g_print("%s\n", gdrop_object_accountinfo(dropObject, NULL, &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 4)
   *   {
   *     g_printerr("[syntex] drop localfile contenttype remotefilename\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_put(dropObject,
   * 				   "dropbox",
   * 				   g_uri_escape_string(argv[3],
   * 						       G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 						       TRUE),
   * 				   argv[1],
   * 				   argv[2],
   * 				   NULL,
   * 				   NULL,
   * 				   NULL,
   * 				   &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 3)
   *   {
   *     g_printerr("[syntex] drop remotefile localfilename [byterange]\n");
   *     exit(EXIT_FAILURE);
   *   }
   * filecontent = gdrop_object_get(dropObject,
   * 				 "dropbox",
   * 				 g_uri_escape_string(argv[1],
   * 						     G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 						     TRUE),
   * 				 (argv[3] ? argv[3] : NULL),
   * 				 NULL,
   * 				 NULL,
   * 				 &error);
   * checkerror(error);
   * g_file_set_contents(argv[2],
   * 		      filecontent->data,
   * 		      filecontent->len,
   * 		      NULL);
   */

  /* 
   * if(argc < 2)
   *   {
   *     g_printerr("[syntex] drop remotefile\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_metadata(dropObject,
   * 					"dropbox",
   * 					g_uri_escape_string(argv[1],
   * 							    G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							    TRUE),
   * 					NULL,
   * 					NULL,
   * 					NULL,
   * 					"true",
   * 					NULL,
   * 					NULL,
   * 					&error));
   * checkerror(error);
   */

  /* 
   * g_print("%s\n", gdrop_object_delta(dropObject,
   * 				     NULL,
   * 				     NULL,
   * 				     &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 2)
   *   {
   *     g_printerr("[syntex] drop remotefile\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_revisions(dropObject,
   * 					 "dropbox",
   * 					 g_uri_escape_string(argv[1],
   * 							     G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							     TRUE),
   * 					 NULL,
   * 					 NULL,
   * 					 &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 3)
   *   {
   *     g_printerr("[syntex] drop remotefile revision\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_restore(dropObject,
   * 				       "dropbox",
   * 				       g_uri_escape_string(argv[1],
   * 							   G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							   TRUE),
   * 				       argv[2],
   * 				       NULL,
   * 				       &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 3)
   *   {
   *     g_printerr("[syntex] drop remotedir query\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_search(dropObject,
   * 				      "dropbox",
   * 				      g_uri_escape_string(argv[1],
   * 							  G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							  TRUE),
   * 				      g_uri_escape_string(argv[2], NULL, TRUE),
   * 				      NULL,
   * 				      "true",
   * 				      NULL,
   * 				      &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 2)
   *   {
   *     g_printerr("[syntex] drop remotefile\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_shares(dropObject,
   * 				      "dropbox",
   * 				      g_uri_escape_string(argv[1],
   * 							  G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							  TRUE),
   * 				      NULL,
   * 				      "true",
   * 				      &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 2)
   *   {
   *     g_printerr("[syntex] drop remotefile\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_media(dropObject,
   * 				     "dropbox",
   * 				     g_uri_escape_string(argv[1],
   * 							 G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							 TRUE),
   * 				     NULL,
   * 				     &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 2)
   *   {
   *     g_printerr("[syntex] drop remotefile\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_copy_ref(dropObject,
   * 					"dropbox",
   * 					g_uri_escape_string(argv[1],
   * 							    G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							    TRUE),
   * 					&error));
   * checkerror(error);
   */

  /* 
   * if(argc < 3)
   *   {
   *     g_printerr("[syntex] drop remotefile localfilename [format]\n");
   *     exit(EXIT_FAILURE);
   *   }
   * filecontent = gdrop_object_thumbnails(dropObject,
   * 					"dropbox",
   * 					g_uri_escape_string(argv[1],
   * 							    G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							    TRUE),
   * 					(argv[3] ? argv[3] : NULL),
   * 					"m",
   * 					NULL,
   * 					&error);
   * checkerror(error);
   * g_file_set_contents(argv[2],
   * 		      filecontent->data,
   * 		      filecontent->len,
   * 		      NULL);
   */

  /* 
   * if(argc < 4)
   *   {
   *     g_printerr("[syntex] drop localfile contenttype remotefilename\n");
   *     exit(EXIT_FAILURE);
   *   }
   * FILE *localfile = g_fopen(argv[1], "r");
   * gchar chunk[CHUNKLENGTHMAX];
   * gsize chunklength = 0;
   * gchar *response = NULL;
   * gchar *upload_id = NULL;
   * gchar *offset = NULL;
   * JsonParser *parser = NULL;
   * JsonNode *node = NULL;
   * JsonNode *resultNode = NULL;
   * 
   * parser = json_parser_new();
   * while(TRUE)
   *   {
   *     memset(chunk, 0, CHUNKLENGTHMAX);
   *     chunklength = fread(chunk, 1, CHUNKLENGTHMAX, localfile);
   *     response = gdrop_object_chunked_upload(dropObject,
   * 					     chunk,
   * 					     chunklength,
   * 					     argv[2],
   * 					     upload_id,
   * 					     offset,
   * 					     &error);
   *     checkerror(error);
   *     g_free(upload_id);
   *     g_free(offset);
   * 
   *     json_parser_load_from_data(parser, response, -1, NULL);
   *     node = json_parser_get_root(parser);
   * 
   *     resultNode = json_path_query("$.upload_id", node, NULL);
   *     upload_id = g_strdup(json_array_get_string_element(json_node_get_array(resultNode), 0));
   * 
   *     resultNode = json_path_query("$.offset", node, NULL);
   *     offset = g_strdup_printf("%ld", json_array_get_int_element(json_node_get_array(resultNode), 0));
   * 
   *     if(chunklength < CHUNKLENGTHMAX)
   * 	break;
   *   }
   * g_print("%s\n", gdrop_object_commit_chunked_upload(dropObject,
   * 						     "dropbox",
   * 						     argv[3],
   * 						     NULL,
   * 						     NULL,
   * 						     NULL,
   * 						     upload_id,
   * 						     &error));
   * checkerror(error);
   * g_object_unref(parser);
   * fclose(localfile);
   */

  /* 
   * if(argc < 3)
   *   {
   *     g_printerr("[syntex] drop remotesource remotedest\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_copy(dropObject,
   * 				    "dropbox",
   * 				    g_uri_escape_string(argv[1],
   * 							G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							TRUE),
   * 				    g_uri_escape_string(argv[2],
   * 							G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							TRUE),
   * 				    NULL,
   * 				    NULL,
   * 				    &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 2)
   *   {
   *     g_printerr("[syntex] drop remotepath\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_create_folder(dropObject,
   * 					     "dropbox",
   * 					     g_uri_escape_string(argv[1],
   * 								 G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 								 TRUE),
   * 					     NULL,
   * 					     &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 2)
   *   {
   *     g_printerr("[syntex] drop remotepath\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_delete(dropObject,
   * 				      "dropbox",
   * 				      g_uri_escape_string(argv[1],
   * 							  G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							  TRUE),
   * 				      NULL,
   * 				      &error));
   * checkerror(error);
   */

  /* 
   * if(argc < 3)
   *   {
   *     g_printerr("[syntex] drop remotesource remotedest\n");
   *     exit(EXIT_FAILURE);
   *   }
   * g_print("%s\n", gdrop_object_move(dropObject,
   * 				    "dropbox",
   * 				    g_uri_escape_string(argv[1],
   * 							G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							TRUE),
   * 				    g_uri_escape_string(argv[2],
   * 							G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   * 							TRUE),
   * 				    NULL,
   * 				    &error));
   * checkerror(error);
   */

  /* 
   * GMainLoop *mainLoop = NULL;
   * mainLoop = g_main_loop_new(NULL, TRUE);
   * 
   * /\* 
   *  * if(argc < 3)
   *  *   {
   *  *     g_printerr("[syntex] drop remotefile localfilename [byterange]\n");
   *  *     exit(EXIT_FAILURE);
   *  *   }
   *  * gdrop_object_get_async(dropObject,
   *  * 			 "dropbox",
   *  * 			 g_uri_escape_string(argv[1],
   *  * 					     G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   *  * 					     TRUE),
   *  * 			 (argv[3] ? argv[3] : NULL),
   *  * 			 NULL,
   *  * 			 NULL,
   *  * 			 NULL,
   *  * 			 gdrop_object_get_async_callback,
   *  * 			 argv[2]);
   *  *\/
   * 
   * /\* 
   *  * if(argc < 4)
   *  *   {
   *  *     g_printerr("[syntex] drop localfile contenttype remotefilename\n");
   *  *     exit(EXIT_FAILURE);
   *  *   }
   *  * gdrop_object_put_async(dropObject,
   *  * 			 "dropbox",
   *  * 			 g_uri_escape_string(argv[3],
   *  * 					     G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   *  * 					     TRUE),
   *  * 			 argv[1],
   *  * 			 argv[2],
   *  * 			 NULL,
   *  * 			 NULL,
   *  * 			 NULL,
   *  * 			 NULL,
   *  * 			 gdrop_object_put_async_callback,
   *  * 			 NULL);
   *  *\/
   * 
   * /\* 
   *  * if(argc < 3)
   *  *   {
   *  *     g_printerr("[syntex] drop remotefile localfilename [format]\n");
   *  *     exit(EXIT_FAILURE);
   *  *   }
   *  * gdrop_object_thumbnails_async(dropObject,
   *  * 				"dropbox",
   *  * 				g_uri_escape_string(argv[1],
   *  * 						    G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   *  * 						    TRUE),
   *  * 				(argv[3] ? argv[3] : NULL),
   *  * 				"m",
   *  * 				NULL,
   *  * 				NULL,
   *  * 				gdrop_object_thumbnails_async_callback,
   *  * 				argv[2]);
   *  *\/
   * 
   * /\* 
   *  * if(argc < 2)
   *  *   {
   *  *     g_printerr("[syntex] drop remotefile\n");
   *  *     exit(EXIT_FAILURE);
   *  *   }
   *  * gdrop_object_metadata_async(dropObject,
   *  * 			      "dropbox",
   *  * 			      g_uri_escape_string(argv[1],
   *  * 						  G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   *  * 						  TRUE),
   *  * 			      NULL,
   *  * 			      NULL,
   *  * 			      NULL,
   *  * 			      "true",
   *  * 			      NULL,
   *  * 			      NULL,
   *  * 			      NULL,
   *  * 			      gdrop_object_metadata_async_callback,
   *  * 			      NULL);
   *  *\/
   * 
   * /\* 
   *  * if(argc < 3)
   *  *   {
   *  *     g_printerr("[syntex] drop remotedir query\n");
   *  *     exit(EXIT_FAILURE);
   *  *   }
   *  * gdrop_object_search_async(dropObject,
   *  * 			    "dropbox",
   *  * 			    g_uri_escape_string(argv[1],
   *  * 						G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   *  * 						TRUE),
   *  * 			    g_uri_escape_string(argv[2], NULL, TRUE),
   *  * 			    NULL,
   *  * 			    "true",
   *  * 			    NULL,
   *  * 			    NULL,
   *  * 			    gdrop_object_search_async_callback,
   *  * 			    NULL);
   *  *\/
   * 
   * /\* 
   *  * gdrop_object_delta_async(dropObject,
   *  * 			   NULL,
   *  * 			   NULL,
   *  * 			   NULL,
   *  * 			   gdrop_object_delta_async_callback,
   *  * 			   NULL);
   *  *\/
   * 
   * /\* 
   *  * if(argc < 2)
   *  *   {
   *  *     g_printerr("[syntex] drop remotefile\n");
   *  *     exit(EXIT_FAILURE);
   *  *   }
   *  * gdrop_object_revisions_async(dropObject,
   *  * 			       "dropbox",
   *  * 			       g_uri_escape_string(argv[1],
   *  * 						   G_URI_RESERVED_CHARS_ALLOWED_IN_PATH,
   *  * 						   TRUE),
   *  * 			       NULL,
   *  * 			       NULL,
   *  * 			       NULL,
   *  * 			       gdrop_object_revisions_async_callback,
   *  * 			       NULL);
   *  *\/
   * 
   * g_main_loop_run(mainLoop);
   */

  return 0;
}
